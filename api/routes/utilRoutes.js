'use strict';
module.exports = function(app , connect , logger) {
    var util = require("../controllers/utilController");
    util.setDatabaseConnect(connect , logger);

    app.route('/util/create_inquiry')
    .post(util.create_inquiry);

    app.route('/util/create_estimate')
    .post(util.create_estimate);

    app.route('/util/get_inquiry_list')
    .post(util.get_inquiry_list);

    app.route('/util/get_estimate_list')
    .post(util.get_estimate_list);

    app.route('/util/get_faq_list')
    .post(util.get_faq_list);

    app.route('/util/get_notice_list')
    .post(util.get_notice_list);

    app.route('/util/get_estimate_info')
    .post(util.get_estimate_info);
}