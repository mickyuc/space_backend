'use strict';
module.exports = function(app , connect , iamporter , logger) {
    var user = require("../controllers/userController");
    user.setDatabaseConnect(connect , iamporter , logger);
    
    app.route('/user/signin')
    .post(user.signin);

    app.route('/user/signup')
    .post(user.signup);
    
    app.route('/user/sendcode')
    .post(user.sendcode);

    app.route('/user/verifycode')
    .post(user.verifycode);
    
    app.route('/user/find_password')
    .post(user.find_password);

    app.route('/user/change_password')
    .post(user.change_password);
    
    app.route('/user/register_address')
    .post(user.register_address);
    
    app.route('/user/sns_signup')
    .post(user.sns_signup);

    app.route('/user/update_profile')
    .post(user.update_profile);

    app.route('/user/check_sns')
    .post(user.check_sns);

    app.route('/user/register_playerid')
    .post(user.register_playerid);

    app.route('/user/log_out')
    .post(user.log_out);

      app.route('/user/get_notice')
    .post(user.get_notice);

      app.route('/user/test')
    .post(user.test);
}