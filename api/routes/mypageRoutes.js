'use strict';
module.exports = function(app , connect , iamporter , logger) {
    var mypage = require("../controllers/mypageController");
    mypage.setDatabaseConnect(connect , iamporter , logger);
    //쿠폰
    app.route('/mypage/get_coupon_list')
    .post(mypage.get_coupon_list);
    //개인정보 수정
    app.route('/mypage/update_userinfo')
    .post(mypage.update_userinfo);
    //비밀번호 변경
    app.route('/mypage/modify_password')
    .post(mypage.modify_password);
    //결제카드 관리
    app.route('/mypage/update_cardinfo')
    .post(mypage.update_cardinfo);
    //결제 내역
    app.route('/mypage/get_transaction_history')
    .post(mypage.get_transaction_history);
    //카드정보
    app.route('/mypage/get_cardinfo')
    .post(mypage.get_cardinfo);
    //은행정보
    app.route('/mypage/get_bank_list')
    .post(mypage.get_bank_list);
    app.route('/mypage/register_refund_account')
    .post(mypage.register_refund_account);
    app.route('/mypage/get_refund_info')
    .post(mypage.get_refund_info);
}
