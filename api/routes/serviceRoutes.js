'use strict';
module.exports = function(app , connect , iamporter , logger) {
    var service = require("../controllers/serviceController");
    service.setDatabaseConnect(connect , iamporter , logger);

    //서비스 요청
    app.route('/service/request')
    .post(service.request);

    //메인 요청
    app.route('/service/getHomeData')
    .post(service.getHomeData);

    //박스 리스트 요청
    app.route('/service/getBoxList')
    .post(service.getBoxList);

    //보관함 정보 가져오기
    app.route('/service/get_boxstorage_info')
    .post(service.get_boxstorage_info);

    //
    app.route('/service/return_request')
    .post(service.return_request);

    //
    app.route('/service/get_time_list')
    .post(service.get_time_list);

    //
    app.route('/service/cancel_request')
    .post(service.cancel_request);

    //
    app.route('/service/get_available_box_list')
    .post(service.get_available_box_list);

    //
    app.route('/service/get_goods_list')
    .post(service.get_goods_list);

    // 재보관 박스개수
    app.route('/service/get_empty_box')
    .post(service.getReusableCount);

    // 보관종료
    app.route('/service/finish')
    .post(service.completeSave);

    // 기간내 재보관
    app.route('/service/restore_in_period')
    .post(service.resendRequest);

    //보관정보 요청
    app.route('/service/storage_detail_info')
    .post(service.getStorageDetailInfo);

    // 개별물건 찾기
    app.route('/service/finish_good')
    .post(service.finishGoods);

    // 탈퇴하기
    app.route('/service/remove_account')
    .post(service.removeAccount);

    //
    app.route('/service/cancel_box')
    .post(service.cancel_box);
}
