'use strict';
const cron = require('node-cron');
const global = require('./config/global');
const User_Model = require('./models/users_model');
const space_model = require('./models/space_model');
const Payment_Model = require('./models/payment_model');
const mysqlConnector = space_model.connect;
const iamporter = space_model.iamporter;
var payment = require('./controllers/paymentController');

User_Model.setConnect(mysqlConnector);
//Payment_Model.setConnect(mysqlConnector);
payment.setConnect(mysqlConnector , iamporter);

var set_disable = function() {
    var today = global.getDashTodayString();
    var query = "SELECT request.* " +
    " FROM request " +
    " WHERE TIMESTAMPDIFF(DAY , DATE_FORMAT(FROM_UNIXTIME(`UPDTIME`), '%Y-%m-%d') , '2019-10-28') >= 7 " +
    " AND request.`STATUS` = 'PROGRESS' AND ( request.`SDETAIL` = 'SHIPPING' OR request.`SDETAIL` = 'ARRANGE') ";

    mysqlConnector.query(query , function(err , results) {
        if(err) {
            console.log(err);
            return;
        }
        results = JSON.stringify(results);
        results = JSON.parse(results);

        for(let index = 0; index < results.length; index ++) {
            var sql = "UPDATE request SET UPDTIME = '" + global.getCurrentTime() + "' ";
            sql += " , " + "`CONDITION` = 'Returning_Impropriety' ";
            sql += " WHERE ID = " + results[index].ID;
            mysqlConnector.query(sql, function(err, result){
                if(err) {
                    console.log(err);
                }
                else {
                    var payment_sql = " SELECT * " +
                    " FROM payment_log " +
                    " WHERE TRANS_NUM = '" + results[index]['PAYMENT_MERCHANT_ID'] + "' ";
                    mysqlConnector.query(payment_sql , function(err , payment_log) {
                        payment_log = JSON.stringify(payment_log);
                        payment_log = JSON.parse(payment_log);
                        var note = '{"type": 8 , "cost" : "' + (parseInt(payment_log[0]['COST']) - 10000) + '"}';
                        var paymentObj = {
                            "STATUS": "PROGRESS" ,
                            "SDETAIL": "ARRANGE" ,
                            "PAY_DESC": "환불(회송불가)" ,
                            "COST": (parseInt(payment_log[0]['COST']) - 10000) ,
                            "NOTE": note ,
                            "ORDER_ID": results[index]['PAYMENT_MERCHANT_ID'],
                            "IMP_UID": payment_log[0]['MERCHANT_ID']
                        }
                        payment.refundProcess(req , res , user_info[0] , paymentObj);
                    })
                }
            });
        }
    });
}
var process_finish = function() {
    var today = global.getDashTodayString();
    var query = "SELECT * FROM request A " +
    "LEFT JOIN request_detail B ON B.REQUEST_ID = A.ID " +
    "WHERE A.DEL_YN = 'N' AND B.DEL_YN = 'N' AND B.MANAGE_STATUS = 4 AND B.GOODS_COUNT = 0 AND DATE_ADD(B.END_DATE, INTERVAL 1 DAY) = '" + today + "'";
    mysqlConnector.query(query , function(err , results) {
        if(err) {
            console.log(err);
            return;
        }
        results = JSON.stringify(results);
        results = JSON.parse(results);

        for(let index = 0; index < results.length; index ++) {
            var rdID = results[index].ID;
            var sql = "UPDATE request_detail SET UPDTIME = '" + global.getCurrentTime() + "' ";
            sql += " , " + "MANAGE_STATUS = 5 ";
            sql += " WHERE ID = " + rdID;
            mysqlConnector.query(sql, function(err, result){
                if(err) {
                    console.log(err);
                }
                else {
                    console.log("process done!!!");
                }
            });
        }
    });
}

function main() {
    var today = global.getDashTodayString();
    var query = "SELECT * FROM request A " +
    "LEFT JOIN request_detail B ON B.REQUEST_ID = A.ID " +
    "WHERE A.DEL_YN = 'N' AND B.DEL_YN = 'N' AND B.MANAGE_STATUS = 4 AND B.GOODS_COUNT > 0 AND DATE_ADD(B.END_DATE, INTERVAL 1 DAY) = '" + today + "'";
    mysqlConnector.query(query, function(err, results){
        if(err) {
            console.log(err);
            return;
        } else {
            results = JSON.stringify(results);
            results = JSON.parse(results);

            var process = function(i) {
                if(i < results.length) {
                    var sql = "UPDATE request_detail SET UPDTIME = '" + global.getCurrentTime() + "' ";
                    var rdID = results[i].ID;
                    var userId = results[i].USER_ID;

                    sql += " , " + "START_DATE = '" + today + "' ";
                    sql += " , " + "END_DATE = DATE_ADD('" + today + "', INTERVAL 1 MONTH) ";
                    sql += " , " + "STORAGE_MONTHS = 1 ";
                    sql += " , " + "AUTO_NEW_MONTHS = AUTO_NEW_MONTHS + 1 ";
                    sql += " WHERE ID = " + rdID;

                    mysqlConnector.query(sql, function(err, result){
                        if(err) {
                            console.log(err);
                            return;
                        } else {
                            User_Model.getUserList({"ID": userId})
                            .then((user_info) => {
                                if(user_info.length == 1) {
                                    var desc = {
                                        "auto_pay_price" : 10000 ,
                                        "box_id" : results[i].BOXID ,
                                        "type" : 4
                                    };
                                    var paymentObj = {
                                        "STATUS": "STORAGE" ,
                                        "SDETAIL": "STORAGE" ,
                                        "PAY_DESC": "자동연장신청" ,
                                        "COST": 10000 ,
                                        "NOTE": JSON.stringify(desc)
                                    };
                                    var res = {};
                                    var req = {
                                        "body" : {
                                            "data" : {
                                                "card_num" : user_info[0]['CARD_NUM'] ,
                                                "card_mm" : user_info[0]['CARD_MM'] ,
                                                "card_yy" : user_info[0]['CARD_YY'] ,
                                                "card_pwd" : user_info[0]['CARD_PWD'] ,
                                                "card_birth_no" : user_info[0]['BIRTH_NO'] ,
                                                "api_token" : user_info[0]['API_TOKEN'] ,
                                                "cost" : 10000
                                            }
                                        }
                                    }
                                    payment.paymentProcess(req , res ,  user_info[0] , paymentObj)
                                    .then((merchant_order_id) => {
                                        process(i+1);
                                        return;
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                        return;
                                    })
                                }
                                else {
                                    console.log('Failed');
                                    return;
                                }
                                /*if(user_info.length == 1) {
                                    Payment_Model.createNewPaymentOrderid()
                                    .then((trans_num) => {
                                        Payment_Model.insert({
                                            "USER_ID": userId ,
                                            "STATUS": "PROGRESS",
                                            "SDETAIL": "SHIPPING" ,
                                            "PAY_DESC": "자동연장신청" ,
                                            "COST": "10000" ,
                                            "TRANS_NUM": trans_num ,
                                            "NOTE": ""
                                        })
                                        .then((result) => {
                                            var customer_uid = global.generateCustomerUid(user_info[0]['USERID'] , userId , user_info[0]['CARD_NUM']);
                                            iamporter.createSubscription({
                                                'customer_uid': customer_uid,
                                                'card_number': user_info[0]['CARD_NUM'],
                                                'expiry': "20" + user_info[0]['CARD_YY'] + '-' + user_info[0]['CARD_MM'],
                                                'birth': user_info[0]['BIRTH_NO'],
                                                'pwd_2digit': user_info[0]['CARD_PWD']
                                            }).then(result => {
                                                if(result.status == 200) {
                                                    process(i+1);
                                                }
                                                console.log(result.message);
                                                return;
                                            }).catch(err => {
                                                console.log(err.message);
                                                return;
                                            });
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                            return;
                                        });
                                    })
                                }
                                else {
                                    console.log('Failed');
                                    return;
                                }
                                */
                            })
                        }
                    })
                } else {
                    console.log('One process is finished');
                    return;
                }
            }
            process(0);
        }
    })
}

var sendNotification = function(data) {
    var headers = {
        "Content-Type": "application/json; charset=utf-8"
    };
    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };
    var https = require('https');
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
        console.log("Response:");
        console.log(JSON.parse(data));
        });
    });
    req.on('error', function(e) {
        console.log("ERROR:");
        console.log(e);
      });
    req.write(JSON.stringify(data));
    req.end();
}

function push_notification() {
    var sql = "SELECT B.`PLAYER_ID`  " +
    "FROM request_detail A " +
    "LEFT JOIN users B " +
    "ON B.ID = A.`USER_ID` " +
    "WHERE A.MANAGE_STATUS = 4 " +
    " AND DATEDIFF(END_DATE , CURDATE()) > 15 GROUP BY A.`USER_ID` ";

    mysqlConnector.query(sql, function(err, result){
        result = JSON.stringify(result);
        result = JSON.parse(result);
        if(result.length < 1)
            return;
        var include_player_ids = [];
        var i = 0;
        for(i = 0; i < result.length; i ++) {
            if( !(result[0]['PLAYER_ID'] == null || result[0]['PLAYER_ID'] == '') )
                include_player_ids.push(result[0]['PLAYER_ID']);
        }
        var message = {
            app_id: "9dc5d130-ec07-4ed1-bab5-a9cabde7bc24",
            "contents": {"en": "회원님의 보관종료일이 15일이내로 도래하였습니다. 보관종료일 도래전 반드시 [전체물품 찾기]를 진행해주세요. 잔여물품이 있는 경우 1달 자동결제 됩니다." , "kr": "회원님의 보관종료일이 15일이내로 도래하였습니다. 보관종료일 도래전 반드시 [전체물품 찾기]를 진행해주세요. 잔여물품이 있는 경우 1달 자동결제 됩니다."} ,
            include_player_ids: include_player_ids,
            "headings": {"en": "[보관종료일 도래]" , "kr": "[보관종료일 도래]"} ,
            "data": {"page": "save_box"}
        }
        sendNotification(message);
    });
}
cron.schedule('0 0 */1 * *', () => {
    console.log("Running a schedule task every 1 day");
    main();
    push_notification();
      set_disable();
      process_finish();
})
//push_notification();
//set_disable();
