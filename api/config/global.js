const aligoapi = require('aligoapi')
, settings = require('../config/settings')
, axios = require('axios') 
, querystring = require('querystring');

var AuthData = {
    key: settings.aligoApiKey,
    // 이곳에 발급받으신 api key를 입력하세요
    user_id: settings.aligoUserid,
    // 이곳에 userid를 입력하세요
}
/*
success response
{
	    "result_code": 1
	    "message": ""
	    "msg_id": 123456789
	    "success_cnt": 2
	    "error_cnt": 0
	    "msg_type": "SMS"
}
*/
/*
fail response
{
	    "result_code": -101
	    "message": "인증오류입니다."
}
*/
function send(receiver , verify_code) {
    let message = '[여유공간] 안녕하세요 고객님, 고객님의 인증번호는 [' + verify_code + ']입니다. 회원가입에 감사드립니다.'
    var req = {
        "body" : {
            "sender": settings.aligoSender ,
            "receiver": receiver ,
            "msg": message ,
            "msg_type": "SMS"
        } ,
        "headers" : {
            "content-type" : "application/json;charset=UTF-8"
        }
    }
    return new Promise((resolve , reject) => {
        aligoapi.send(req , AuthData)
        .then((r) => {
            resolve(r);
        })
        .catch((e) => {
            reject(e);
        })
    })
}

function generateRandomString(length = 24)
{
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function generateVerifyNumber(length = 4) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getCurrentTime() {
    return Math.floor(Date.now() / 1000)    
}

function getCurrentDateTimeStamp() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return Math.floor(Date.parse(yyyy+"-" + mm + "-" + dd + "T00:00:00") / 1000);
}

function getTodayString() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return yyyy + mm + dd;
}

function getBoxString() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yy = today.getFullYear().toString().substr(-2);
    return yy + mm + dd;
}

function getDashTodayString() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return yyyy + "-" + mm + "-" + dd;
}

function zeroFill( number , width ) {
    width -= number.toString().length;
    if ( width > 0 ) {
        return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + ""; // always return a string
}

function generateCustomerUid(userid , userno , cardnum) {
    var res = cardnum.split('-');
    return zeroFill( userno , 8 ) + '_' + res[1] + res[2] + res[3] + '_' + generateRandomString(16);
}

async function authenticate() {
    return axios.post(
      'https://api.iamport.kr/users/getToken',
      {
        imp_key: settings.api_key,
        imp_secret: settings.api_secret,
      },
    );
}

async function checkBankHolder(accessToken , bankCode , bankAccountNumber) {
    const query = querystring.stringify({
            bank_code: bankCode,
            bank_num: bankAccountNumber,
    });
    return axios.get(
        `${settings.apiBaseUrl}/vbanks/holder?${query}`,
        {
          headers: {
            Authorization: `Bearer ${accessToken}`, // 인증 요청이므로 헤더에 토큰을 넘겨준다.
          },
        },
      ).catch((error) => {
        /**
         * handling iamport error
         */
        const clientErrors = [400, 404];
        // 400, 404 에러는 유저가 값을 누락한 경우이므로 다른 에러 클래스로 처리한다.
        if (clientErrors.includes(error.response.status)) {
          throw new String(error.response.data.message);
        }
        throw new String('iamport api 에러');
      });
}


var sendNotification = function(data) {
    var headers = {
        "Content-Type": "application/json; charset=utf-8"
    };
    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };
    var https = require('https');
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
        console.log("Response:");
        console.log(JSON.parse(data));
        });
    });
    req.on('error', function(e) {
        console.log("ERROR:");
        console.log(e);
      });
    req.write(JSON.stringify(data));
    req.end();
}

module.exports = {
    generateRandomString , getCurrentTime , generateCustomerUid , generateVerifyNumber , getCurrentDateTimeStamp , getTodayString, getDashTodayString , send , authenticate , checkBankHolder , sendNotification , getBoxString
}
