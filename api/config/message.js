'use strict';
var message = {
    invalidParameter: "입력파라메터들을 다시 확인해주세요." ,
    loginFailed: "로그인 실패하였습니다." ,
    apiFailed: "API 호출이 실패하였습니다." ,
    duplicateId: "이미 존재하는 아이디입니다." ,
    duplicateEmail: "이미 가입된 이메일입니다." ,
    duplicateMobile: "사용 중인 휴대폰 번호입니다.",
    signupFailed: "회원가입 실패하였습니다.",
    curpwdcheckFailed: "현재 비밀번호가 틀렸습니다.",
    confirmpwdcheckFailed: "비밀번호가 일치하지 않습니다.",
    pwdchangeFailed: "비밀번호 변경이 실패하였습니다.",
    noUser: "유저정보가 존재하지 않습니다." ,
    exitUser: "탈퇴된 유저입니다.",
    cardinfoFailed: "카드정보를 다시 확인해주세요." , 

    sendcodeFailed: "인증번호발송이 실패하였습니다." ,
    verifyFailed: "인증번호가 일치하지 않습니다." ,
    nonVerifiedMobile: "인증되지 않은 휴대폰번호 입니다." ,
    curpasswordWrong: "현재비밀번호가 맞지 않습니다.",
    
    /* 비밀번호 찾기에서 */
    emailVerifiedFailed: "이메일 인증이 실패하였습니다." ,    

    paymentStoreFailed: "결제모듈에서 문제가 발생하였습니다." ,
    requestStoreFailed: "서비스 신청에서 문제가 발생하였습니다.",

    insufficientBoxFailed: "박스개수가 충분하지 않습니다.",
    
    cancelFailed: "신청접수에 실패하였습니다.",
    
    alreadyRegisteredMessage: "이미 등록된 정보입니다"

}
module.exports = message


/*
* 비밀번호가 성과적으로 변경되었습니다.
* 카드정보가 성과적으로 변경되었습니다.
*/