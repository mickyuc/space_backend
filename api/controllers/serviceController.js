'use strict';
require('datejs');
const { Iamporter, IamporterError } = require('iamporter');
const { exitUser } = require('../config/message');
var User_Model = require('../models/users_model')
, Banner_Model = require('../models/banner_model')
, Request_Model = require('../models/request_model')
, Box_Model = require('../models/box_model')
, Payment_Model = require('../models/payment_model')
, message = require('../config/message')
, settings = require('../config/settings')
, Goods_Model = require('../models/goods_model')
, Coupon_Model = require('../models/coupon_model')
, Find_Model = require('../models/find_model')
, response = {
    "status":"success" ,
    "errMsg": ""
}
, global = require('../config/global')
, payment = require('./paymentController');

var mysqlConnect = null , iamporter = null , logger = null;
exports.setDatabaseConnect = function(connect , paymentConnect , logger_param) {
    mysqlConnect = connect;
    iamporter = paymentConnect;
    logger = logger_param;
    User_Model.setConnect(mysqlConnect);
    Banner_Model.setConnect(mysqlConnect);
    Request_Model.setConnect(mysqlConnect);
    Box_Model.setConnect(mysqlConnect);
    Goods_Model.setConnect(mysqlConnect);
    Payment_Model.setConnect(mysqlConnect);
    Coupon_Model.setConnect(mysqlConnect);
    Find_Model.setConnect(mysqlConnect);
    payment.setConnect(mysqlConnect , iamporter);
}
var request_process3 = function(req , res , response , user_info , request_insert_id , player_id) {
    Box_Model.createBoxId()
    .then((box_num) => {
        for(let i = 0; i < req.body.data.space_list.length; i ++) {
            for(let j = 0; j < req.body.data.space_list[i]['amount']; j ++)  {

                var prefix_boxid = 'B';
                if(req.body.data.space_list[i]['box_no'] == '1')
                    prefix_boxid = 'B';
                else if(req.body.data.space_list[i]['box_no'] == '2')
                    prefix_boxid = 'A';
                else
                    prefix_boxid = 'C';

                Request_Model.insertRequestDetail({
                    "REQUEST_ID": request_insert_id ,
                    "BOXID": prefix_boxid + global.getBoxString() + String(box_num).padStart(3, '0') ,
                    "PRICE": 10000 * parseInt(req.body.data.space_list[i]['months']),
                    "GOODS_COUNT": 0,
                    "MANAGE_STATUS": 0,
                    "STORAGE_MONTHS": req.body.data.space_list[i]['months'],
                    "USER_ID": user_info[0]['ID'],
                    "NOTE": "",
                    "TRANS_NUMBER": "",
                    "BOX_NO": req.body.data.space_list[i]['box_no']
                });
                box_num ++;
            }
        }
        if(req.body.data.coupon.length > 0) {
            for(let i = 0; i < req.body.data.coupon.length; i ++ )
                Coupon_Model.update({
                    "USED" : req.body.data.coupon[i].used ,
                    "ISSUED" : req.body.data.coupon[i].issued
                } , {
                    "USER_ID" : user_info[0]['ID'] ,
                    "COUPON_ID" : req.body.data.coupon[i].coupon_id
                });
        }
        var include_player_ids = [];
        include_player_ids.push(player_id);
        var message = {
            app_id: settings.onesignal_app_id,
            "contents": {"en": "서비스 신청이 완료 되었습니다. 5일안에 보관함이 도착할 예정입니다." , "kr": "서비스 신청이 완료 되었습니다. 5일안에 보관함이 도착할 예정입니다."} ,
            include_player_ids: include_player_ids,
            "headings": {"en": "[서비스 신청완료]" , "kr": "[서비스 신청완료]"} ,
            "data": {"page": "save_box"}
        }
        global.sendNotification(message);
        response['status'] = 'success';
        response['errMsg'] = '';
        res.json(response);
        return;
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    });
}
var request_process4 = function(req , res , response , request_insert_id, values) {
    for(let i = 0; i < req.body.data.box_count; i ++) {
        Request_Model.updateRequestDetail({
            "REQUEST_ID": request_insert_id,
            "MANAGE_STATUS": 0,
            "TRANS_NUMBER": ""
        }, {
            "ID": values[i].ID
        });
    }

    response['status'] = 'success';
    response['errMsg'] = '';
    res.json(response);
    return;
}
exports.request = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if ( (req.body.data.space_list == undefined || req.body.data.space_list == null || req.body.data.space_list.length == 0) 
    || (req.body.data.desc == undefined || req.body.data.desc == '')
    || (req.body.data.addr1 == undefined || req.body.data.addr1 == '')
    || (req.body.data.addr2 == undefined || req.body.data.addr2 == '')
    || (req.body.data.detail_addr == undefined || req.body.data.detail_addr == '')
    || (req.body.data.card_num == undefined || req.body.data.card_num == '')
    || (req.body.data.card_mm == undefined || req.body.data.card_mm == '')
    || (req.body.data.card_yy == undefined || req.body.data.card_yy == '')
    || (req.body.data.card_birth_no == undefined || req.body.data.card_birth_no == '')
    || (req.body.data.card_pwd == undefined || req.body.data.card_pwd == '')
    || (req.body.data.api_token == undefined || req.body.data.api_token == '')) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    //req.body.data.cost =  1000;
    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length == 1) {
            var paymentObj = {
                "STATUS": "PROGRESS" ,
                "SDETAIL": "SHIPPING" ,
                "PAY_DESC": "보관신청" ,
                "COST": req.body.data.cost ,
                "NOTE": req.body.data.hasOwnProperty('desc') ? req.body.data.desc : ''
            };
            payment.paymentProcess(req , res ,  user_info[0] , paymentObj)
            .then((merchant_order_id) => {
                Request_Model.createTransnumber()
                .then((transport_number) => {
                    Request_Model.insertRequest({
                        "`USER_ID`": user_info[0]['ID'] ,
                        "`ADDRESS`": req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr ,
                        "`TRANS_NUMBER`": transport_number ,
                        "`STATUS`": "PROGRESS" ,
                        "`SDETAIL`": "SHIPPING" ,
                        "`CONDITION`": "Custom_Delivery" ,
                        "`PAYMENT_MERCHANT_ID`": merchant_order_id,
			            //"`PAYMENT_MERCHANT_ID`": "merchant_order_temp",
                        "IS_THING_CHECK" : req.body.data.isThingCheck
                    })
                    .then((request_insert_id) => {
                        request_process3(req , res , response , user_info , request_insert_id , user_info[0]['PLAYER_ID']);
                    })
                    .catch((err) => {
                        logger.error(err);
                        response['status'] = 'fail';
                        response['errMsg'] = message.requestStoreFailed;
                        res.json(response);
                    });
                })
                .catch((err) => {
                    logger.error(err);
                    response['status'] = 'fail';
                    response['errMsg'] = message.apiFailed;
                    res.json(response);
                })
            })
            .catch((err) => {
                logger.error(err);
                response['status'] = 'fail';
                response['errMsg'] = err.errMsg;
                res.json(response);
            }) 
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
var responseHomeData = function(res , response , userid) {
    Promise.all([Request_Model.getMyRequestCount(userid) , Request_Model.getMyStoreBoxCount(userid)])
    .then((values) => {
        response['status'] = 'success';
        response['errMsg'] = '';
        response['data']['requestCount'] = values[0];
        response['data']['boxCount'] = values[1];
        res.json(response);
    })
    .catch((err) => {
        logger.error(err);
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.getHomeData = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    //get banner list
    Banner_Model.getBannerList()
    .then((result) => {
        response['data'] = {};
        response['data'].banner = [];
        for(let i = 0; i < result.length; i ++)
            response['data']['banner'][i] = settings.base_url + result[i]['IMAGE'];
        if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
            //you are not logged in.
            response['status'] = 'success';
            response['errMsg'] = '';
            response['data'].requestCount = 0;
            response['data'].boxCount = 0;
            res.json(response);
            return;
        }
        // you are logged in now. get user id
        User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
        .then((user_info) => {
            if(user_info.length < 1) {
		response['status'] = 'fail';
                response.is_logout = true;
                response['errMsg'] = message.loginFailed;
                response['data'].requestCount = 0;
                response['data'].boxCount = 0;
                res.json(response);
                return;
            }
            else
                responseHomeData(res , response , user_info[0]['ID']);
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        });
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    });
}

exports.getBoxList = async function(req , res) {
    try {
        response = {
            "status":"success" , "errMsg": ""
        };
        let result = await Box_Model.getBoxList();
        response['status'] = 'success';
        response['errMsg'] = '';
        response['data'] = {}; response['data']['box_list'] = [];
        for(let i = 0; i < result.length; i ++)  {
            response['data']['box_list'][i] = {
                "id" : result[i]['ID'] ,
                "name" : result[i]['NAME'] ,
                "width" : result[i]['WIDTH'] ,
                "length" : result[i]['LENGTH'] ,
                "height" : result[i]['HEIGHT'] ,
                "max_weight" : result[i]['MAX_WEIGHT'] ,
                "expect_qty" : result[i]['EXPECT_QTY'] ,
                "image1" : result[i]['IMAGE1'] ,
                "image2" : result[i]['IMAGE2'] ,
                "price" : result[i]['PRICE'] ,
                "type" : 1
            }
        }
        let basic_price = await Box_Model.getBasicPrice();
        response['data']['box_list'].push({
            "id" : 0 ,
            "name" : '기본비용' ,
            "width" : 30 ,
            "length" : 30 ,
            "height" : 30 ,
            "max_weight" : 100 ,
            "expect_qty" : 0 ,
            "image1" : '' ,
            "image2" : '' ,
            "price" : basic_price ,
            "type" : 0
        });
        res.json(response);
    } catch (err) {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    }
}

exports.get_boxstorage_info = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
	
    

    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then(async (user_info) => {
        if(user_info.length) {
            const userid = user_info[0]['ID'];
            const DETAIL_TYPE_MAPPING = {
                SHIPPING: 1,
                ARRANGE: 2,
                COLLECT: 3,
                CHECK: 4
            }
            let storage_list = await Request_Model.getBoxstorageInfo({ userid, status: 4 });
            let finish_list = await Request_Model.getBoxstorageInfo({ userid, status: 5 });
            let remaining_list = await Request_Model.getBoxstorageRemainingInfo({ userid, status: 4 });
            let progress_list = await Request_Model.getBoxstorageProgressInfo({ userid, status:'PROGRESS' });

            let group_progress_list = progress_list.reduce((acc, item) => {
                const key = 'p_' + item.request_id
                if(!acc[key]) acc[key] = { box_list: [] };
                acc[key].type = 1;
                acc[key].detail_type = DETAIL_TYPE_MAPPING[item.detail_type];
                acc[key].box_list.push({
                    box_name: item.box_name,
                    amount: item.amount,
                    goods_count: item.goods_count
                });
                acc[key].request_id = item.request_id;
                return acc;
            }, {});

            response['data'] = {
                data_list: storage_list.map(it => ({
                    type: 3,
                    box_list: [it],
                    total_goods_count: it.goods_count
                })).concat(
                    finish_list.map(it => ({
                        type: 4,
                        box_list: [it],
                        total_goods_count: it.goods_count
                    }))
                ).concat(
                    remaining_list.map(it => ({
                        type: 2,
                        box_list: [it],
                        rest_days: it.rest_days
                    })
                )).concat(Object.keys(group_progress_list).map(key => group_progress_list[key]))
            };
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.exitUser;
            res.json(response);
        }
    }).catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
	response['errCon'] = JSON.stringify(err);
        res.json(response);
    })
}
exports.return_request = async function (req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    
    try {
        const user_info = await User_Model.getUserList({"API_TOKEN" : req.body.data.api_token});
        if(user_info.length) {
            await Request_Model.updateRequest({
                SDETAIL: 'COLLECT' ,
                CONDITION: 'Returning',
                REQUEST_DATE : req.body.data.request_date,
                REQUEST_TIME : req.body.data.request_time
            }, {
                ID: req.body.data.request_id,
                USER_ID: user_info[0]['ID']
            })
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.exitUser;
            res.json(response);
        }
    } catch(err) {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    };
}
exports.get_time_list = async function (req, res) {
    Promise.all([Request_Model.getTimeList(req.body.data.request_date)])
    .then((values) => {
        response['status'] = 'success';
        response['errMsg'] = '';
        response['data']['requestCount'] = values[0];
        console.log(values)
        res.json(response);
    })
    .catch((err) => {
        logger.error(err);
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.cancel_box = async function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    try {
        let user_info = await User_Model.getUserList({"ID" : req.body.data.userid});
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.exitUser;
            res.json(response);
            return;
        }
        let cost = await Request_Model.getRemainCost(req.body.data.boxid);

        if(cost == settings.return_processing_cost) {
            response['status'] = 'success';
            response['errMsg'] = "";
            res.json(response);
        }
        else {
            var note = '{"type": 6 , "boxid" : "' + req.body.data.boxid + '" , "cost" : "' + req.body.data.cost + '"}';
            let request = await Request_Model.getOrderidFromBox(req.body.data.boxid);
            var paymentObj = {
                "STATUS": "PROGRESS" ,
                "SDETAIL": "CHECK" ,
                "PAY_DESC": "환불(반송요청)" ,
                "COST": ( cost - settings.return_processing_cost ) ,
                "NOTE": note ,
                "ORDER_ID": request
            }
            await payment.refundProcess(req , res , user_info[0] , paymentObj);
            response['status'] = 'success';
            response['errMsg'] = "";
            res.json(response);
        }
    } catch(err) {
        logger.error(err);
        if(err instanceof IamporterError) {
            response['status'] = 'fail';
            response['errMsg'] = err.raw.message;
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        }
    };
}
exports.cancel_request = async function (req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    try {
        let user_info = await User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
        if(user_info.length) {
            const request = await Request_Model.getRequestInfo(req.body.data.request_id);
            // Convert PHP time to javascript
            let regtime = request[0].REGTIME * 1000;
            let deadline = new Date().setHours(15, 0, 0, 0);
            let passMoreOneday = false;
            if(deadline - regtime >= 86400000) {
                passMoreOneday = true;
            }
            if( !passMoreOneday ) {
                if (regtime >= deadline) { // Check if registered after today's deadline
                    deadline = new Date(deadline).addDays(1).getTime();
                }
            }
            const payment_log_info = await Payment_Model.getPaymentLogList({"TRANS_NUM" : request[0]['PAYMENT_MERCHANT_ID']})
            if(payment_log_info.length < 1) {
                response['status'] = "fail";
                response['errMsg'] = message.apiFailed;
                res.json(response);
                return;
            }

            let payment_desc = JSON.parse(payment_log_info[0]['NOTE']);
            if(payment_desc.hasOwnProperty('coupon_id') && payment_desc.coupon_id > 0) {
                await Coupon_Model.update({
                    "USED" : 0 ,
                    "ISSUED" : 1
                } , {
                    "USER_ID" : user_info[0]['ID'] ,
                    "COUPON_ID" : payment_desc.coupon_id
                });
            }
            let cancel_amount = 0;
            cancel_amount = parseInt(payment_log_info[0]['COST']);
            let CONDITION = 'RequestCancel_RA';
            if (new Date().getTime() > deadline || passMoreOneday) { // Penalty
                CONDITION = 'RequestCancel_RH';
                if(cancel_amount - settings.payment_cancel_delivery_amount > 0) {
                    var note = '{"type": 5 , "cost" : "' + (cancel_amount - settings.payment_cancel_delivery_amount) + '"}';
                    var paymentObj = {
                        "STATUS": "PROGRESS" ,
                        "SDETAIL": "SHIPPING" ,
                        "PAY_DESC": "환불(신청취소)" ,
                        "COST": cancel_amount - settings.payment_cancel_delivery_amount ,
                        "NOTE": note ,
                        "ORDER_ID": request[0]['PAYMENT_MERCHANT_ID'],
                        "IMP_UID": payment_log_info[0]['MERCHANT_ID']
                    }
                    await payment.refundProcess(req , res , user_info[0] , paymentObj);
                }
            }
            else {
                var note = '{"type": 7 , "cost" : "' + cancel_amount + '"}';
                var paymentObj = {
                    "STATUS": "PROGRESS" ,
                    "SDETAIL": "SHIPPING" ,
                    "PAY_DESC": "결제취소" ,
                    "COST": cancel_amount ,
                    "NOTE": note ,
                    "ORDER_ID": request[0]['PAYMENT_MERCHANT_ID'],
                    "IMP_UID": payment_log_info[0]['MERCHANT_ID']
                }
                await payment.refundProcess(req , res , user_info[0] , paymentObj);
            }
            // Cancel API
            await Request_Model.updateRequest({
                STATUS: 'PROGRESS',
                CONDITION
            }, {
                ID: req.body.data.request_id,
                USER_ID: user_info[0]['ID']
            });
            if(request[0]['RESTORE_YN'] == 'Y') {
                //restore
                var box_ids = request[0]['RESTORE_BOX_IDS'].split(',');
                for(let index = 0; index < box_ids.length; index ++) {
                    Request_Model.updateRequestDetail({
                        "GOODS_COUNT" : 0 ,
                        "MANAGE_STATUS" : 4
                    } , {
                        "ID" : box_ids[index]
                    });
                }
            }
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.exitUser;
            res.json(response);
        }
    } catch(err) {
        console.log(err);
        logger.error(err);
        if(err instanceof IamporterError) {
            response['status'] = 'fail';
            response['errMsg'] = err.raw.message;
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        }
    };
}
exports.get_available_box_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if ( req.body.data.api_token == undefined || req.body.data.api_token == "" ) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }
        Request_Model.getRequestDetailList({
            "USER_ID" : user_info[0]['ID'] ,
            "MANAGE_STATUS" : 4
        })
        .then((result) => {
            response = {
                'status' : 'success' ,
                'errMsg' : '' ,
                'data' : {}
            };
            response['data']['box_list']  = [];
            for( let i = 0; i < result.length; i ++ ) {
                response['data']['box_list'][i] = {
                    "box_no" : result[i]['BOX_NO'] , 
                    "box_id" : result[i]['BOXID']
                }
            }
            res.json(response);
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        })
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.get_goods_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == undefined || req.body.data.api_token == ""
    || req.body.data.box_id == undefined || req.body.data.box_id == "") {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }
        Goods_Model.getGoodsList(user_info[0]['ID'] , req.body.data.box_id)
        .then((result) => {
            response['status'] = 'success';
            response['errMsg'] = '';
            response['data'] = {
                'goods_list' : []
            };
            for( let i = 0; i < result.length; i ++ ) {
                response['data']['goods_list'][i] = {
                    "goods_name": result[i]['GOODS_NAME'] ,
                    "goods_id": result[i]['GOODSID'] ,
                    "start_date": result[i]['START_DATE'] ,
                    "image": result[i]['IMAGE'] ,
                    "goods_no": result[i]['ID'],
                    "status" : result[i]['STATUS']
                }
            }
            res.json(response);
            return;
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        })
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })


}

exports.getStorageDetailInfo = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };

    if ((req.body.data.api_token == undefined || req.body.data.api_token == '')
    || (req.body.data.box_id == undefined || req.body.data.box_id == '')) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then( user_info => {
        if(user_info.length == 1) {
            var user_id = user_info[0]['ID'];
            var box_id = req.body.data.box_id;
            Request_Model.getStorageDetailInfo(user_id, box_id)
            .then((result) => {
                response['status'] = 'success';
                response['errMsg'] = '';
                response['data'] = {}; 
                response['data']['box_list'] = [];
                for(let i = 0; i < result.length; i ++)  {
                    response['data']['box_list'][i] = {
                        "box_name" : result[i]['NAME'] , 
                        "box_id" : result[i]['BOXID'] , 
                        "start_date" : result[i]['START_DATE'] ,
                        "end_date" : result[i]['END_DATE'] ,
                        "storage_count" : result[i]['STORAGE_COUNT'] ,
                        "delivery_count" : result[i]['DELIVERY_COUNT'] ,
                        "finish_count" : result[i]['FINISH_COUNT'] ,
                        "box_count" : 1 , 
                        "cost" : 10000
                    }
                }
                res.json(response);
            })
            .catch((err) => {
                logger.error(err);
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            })
        } else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response); 
        }
    })
    .catch( err => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.getReusableCount = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }
        var userid = user_info[0].ID;
        Promise.all([Request_Model.getReusableCount(userid)])
        .then((values) => {
            response['status'] = 'success';
            response['errMsg'] = '';
            response['data'] = {
                "box_count": values[0]
            };
            res.json(response);
        })
        .catch((err) => {
            logger.error(err);
            response['errMsg'] = message.apiFailed;
            res.json(response);
        });
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    });
}

exports.completeSave = async function(req, res) {
    if(req.body.data == undefined
    || req.body.data.addr1 == '' || req.body.data.addr1 == undefined
    || req.body.data.addr2 == '' || req.body.data.addr2 == undefined
    || req.body.data.detail_addr == '' || req.body.data.detail_addr == undefined
    || req.body.data.box_id == '' || req.body.data.box_id == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    response = {
        "status":"success" , "errMsg": ""
    };

    try {
        let user_info = await User_Model.getUserList({"API_TOKEN":req.body.data.api_token});
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }
        let goods_list = await Goods_Model.getGoodsListOnly(user_info[0]['ID'] , req.body.data.box_id);
        if(goods_list.length > 0) {
            let find_table_id = await Find_Model.insert({
                "NOTE" : goods_list.length + "개 물건 찾기(보관종료)" ,
                "TRANS_NUMBER" : "" ,
                "BOXID" : req.body.data.box_id ,
                "PRICE" : 0 ,
                "GOODS_COUNT" : goods_list.length ,
                "ADDR" : req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr
            });
            for( let index = 0; index < goods_list.length; index ++ ) {
                await Request_Model.updateRequestDetail({
                    "STATUS": 3,
                    //"ADDRESS": req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr
                    "RESET_YN": "Y",
                    "FIND_TABLE_ID": find_table_id
                }, {
                    "GOODSID":goods_list[index]['GOODSID'],
                    "BOXID": req.body.data.box_id
                }, 'detail_goods');
            }
        }
        await Request_Model.updateRequestDetail({
            GOODS_COUNT: 0,
            MANAGE_STATUS: 5
        }, {
            BOXID : req.body.data.box_id
        });
        res.json(response);
        return;
    } catch(err) {
        console.log(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    }
}

function get_date_string(obj) {
    var mm = obj.getMonth() + 1; // getMonth() is zero-based
    var dd = obj.getDate();

  return [obj.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('-');
}

exports.resendRequest = async function(req, res) {
    if(req.body.data == undefined
    || req.body.data.cost == '' || req.body.data.cost == undefined
    || req.body.data.card_num == '' || req.body.data.card_num == undefined
    || req.body.data.card_mm == '' || req.body.data.card_mm == undefined
    || req.body.data.card_yy == '' || req.body.data.card_yy == undefined
    || req.body.data.card_birth_no == '' || req.body.data.card_birth_no == undefined
    || req.body.data.card_pwd == '' || req.body.data.card_pwd == undefined
    || req.body.data.addr1 == '' || req.body.data.addr1 == undefined
    || req.body.data.addr2 == '' || req.body.data.addr2 == undefined
    || req.body.data.detail_addr == '' || req.body.data.detail_addr == undefined
    || req.body.data.box_count == '' || req.body.data.box_count == undefined
    || req.body.data.box_count <= 0) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    response = {
        "status":"success" , "errMsg": ""
    };

    try {
        let user_info = await User_Model.getUserList({"API_TOKEN":req.body.data.api_token});
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }
        var userid = user_info[0].ID;
        let values = await Request_Model.getReusableList(userid , req.body.data.box_count);
        if(values.length < req.body.data.box_count) {
            response['status'] = 'fail';
            response['errMsg'] = message.insufficientBoxFailed;
            res.json(response);
            return;
        }
        var paymentObj = {
            "STATUS": "PROGRESS" ,
            "SDETAIL": "SHIPPING" ,
            "PAY_DESC": "재보관신청" ,
            "COST": req.body.data.cost ,
            "NOTE": req.body.data.hasOwnProperty('desc') ? req.body.data.desc : ''
        };
        var paymentObj = {
            "STATUS": "PROGRESS" , "SDETAIL": "SHIPPING" , "PAY_DESC": "재보관신청" ,
            "COST": req.body.data.cost , "NOTE": req.body.data.hasOwnProperty('desc') ? req.body.data.desc : ''
        };
        let merchant_order_id = await payment.paymentProcess(req , res ,  user_info[0] , paymentObj);
        let transport_number = await Request_Model.createTransnumber();
        ///  update previous box information
        for(let index = 0; index < values.length; index ++) {
            Request_Model.updateRequestDetail({
                "GOODS_COUNT" : 0 ,
                "MANAGE_STATUS" : 5
            } , {
                "ID" : values[index].ID
            });
        }
        ///
        var box_ids_string = '';
        box_ids_string = values[0].ID;
        for(let index = 1; index < values.length; index ++) {
            box_ids_string += ',' + values[index].ID;
        }
        let new_request_id = await Request_Model.insertRequest({ 
            "`USER_ID`": user_info[0]['ID'] ,
            "`ADDRESS`": req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr ,
            "`TRANS_NUMBER`": transport_number ,
            "`STATUS`": "PROGRESS" ,
            "`SDETAIL`": "SHIPPING" ,
            "`CONDITION`": "Custom_Delivery" ,
            "PAYMENT_MERCHANT_ID": merchant_order_id ,
            "RESTORE_YN": "Y" ,
            "RESTORE_BOX_IDS": box_ids_string
        });
        let box_num = await Box_Model.createBoxId();
        for(let index = 0; index < values.length; index ++) {
            var prefix = 'B' , box_no = '1';
            if(index < parseInt(req.body.data.combined_box_count[0]))
            {
                prefix = 'B';
                box_no = '1';
            }
            else {
                prefix = 'A';
                box_no = '2';
            }

	    await Request_Model.insertRequestDetail({
                "REQUEST_ID": new_request_id ,
                "BOXID": prefix + global.getBoxString() + String(box_num).padStart(3, '0') ,
                "PRICE": 0,
                "GOODS_COUNT": 0,
                "MANAGE_STATUS": 0,
                "STORAGE_MONTHS": values[index]['STORAGE_MONTHS'],
                "USER_ID": user_info[0]['ID'],
                "NOTE": "",
                "TRANS_NUMBER": "",
                "BOX_NO": box_no ,
                "START_DATE" : get_date_string(new Date(values[index]['START_DATE'])) ,
                "END_DATE": get_date_string(new Date(values[index]['END_DATE']))
            });
            box_num ++;
        }

        response = {
            "status":"success" , "errMsg": ""
        };
        res.json(response);
        return;

    } catch(err) {
        console.log(err);
        logger.error(err);
        if(err instanceof IamporterError) {
            response['status'] = 'fail';
            response['errMsg'] = err.raw.message;
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        }
    }
    /*response = {
        "status":"success" , "errMsg": ""
    };
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length == 1) {
            var userid = user_info[0].ID;
            Promise.all([Request_Model.getReusableList(userid, req.body.data.box_count)])
            .then((values) => {
                values = values[0];
                if(values.length < req.body.data.box_count) {
                    response['status'] = 'fail';
                    response['errMsg'] = message.insufficientBoxFailed;
                    res.json(response);
                    return;
                }
                var paymentObj = {
                    "STATUS": "PROGRESS" ,
                    "SDETAIL": "SHIPPING" ,
                                        "PAY_DESC": "재보관신청" ,
                    "COST": req.body.data.cost ,
                    "NOTE": req.body.data.hasOwnProperty('desc') ? req.body.data.desc : ''
                };
                payment.paymentProcess(req , res ,  user_info[0] , paymentObj)
                .then((result) => {
                    Request_Model.createTransnumber()
                    .then((transport_number) => {
                        Request_Model.insertRequest({
                            "`USER_ID`": user_info[0]['ID'] ,
                            "`ADDRESS`": req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr ,
                            "`TRANS_NUMBER`": transport_number ,
                            "`STATUS`": "PROGRESS" ,
                            "`SDETAIL`": "SHIPPING" ,
                            "`CONDITION`": "Custom_Delivery"
                        })
                        .then((request_insert_id) => {
                            request_process4(req , res , response , request_insert_id, values);
                        })
                        .catch((err) => {
                            logger.error(err);
                            response['status'] = 'fail';
                            response['errMsg'] = message.requestStoreFailed;
                            res.json(response);
                        });
                    })
                    .catch((err) => {
                        logger.error(err);
                        response['status'] = 'fail';
                        response['errMsg'] = message.apiFailed;
                        res.json(response);
                    });
                })
                .catch((err) => {
                    logger.error(err);
                    response['status'] = 'fail';
                    response['errMsg'] = err.errMsg;
                    res.json(response);
                });
            })
            .catch((err) => {
                logger.error(err);
                response['errMsg'] = message.apiFailed;
                res.json(response);
            });
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    }); */
}
exports.finishGoods = async function(req, res) {
    if(req.body.data == undefined
        || req.body.data.addr1 == '' || req.body.data.addr1 == undefined
        || req.body.data.addr2 == '' || req.body.data.addr2 == undefined
        || req.body.data.detail_addr == '' || req.body.data.detail_addr == undefined
        || req.body.data.good_ids == undefined || req.body.data.good_ids.length == 0
        || req.body.data.box_id == '' || req.body.data.box_id == undefined) {
            response['status'] = 'fail';
            response['errMsg'] = message.invalidParameter;
            res.json(response);
            return;
    }
    response = {
        "status":"success" , "errMsg": ""
    };
    try {
        let user_info = await User_Model.getUserList({"API_TOKEN":req.body.data.api_token});
        if(user_info.length == 1) {
            let box_info = await Request_Model.getRequestDetailInfo({"BOXID" : req.body.data.box_id});
            if(box_info.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.invalidParameter;
                res.json(response);
                return;
            }
            if(box_info[0]['GOODS_COUNT'] == req.body.data.good_ids.length) {
                var goodIDs = req.body.data.good_ids;
                let find_table_id = await Find_Model.insert({
                    "NOTE" : goodIDs.length + "개 물건 찾기(보관 종료)" ,
                    "TRANS_NUMBER" : "" ,
                    "BOXID" : req.body.data.box_id ,
                    "PRICE" : 0 ,
                    "GOODS_COUNT" : goodIDs.length ,
                    "ADDR" : req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr
                });
                for(var ind = 0; ind < goodIDs.length; ind++) {
                    await Request_Model.updateRequestDetail({
                        "STATUS": 3,
                        "RESET_YN": "Y",
                        "FIND_TABLE_ID": find_table_id
                    }, {
                        "GOODSID": goodIDs[ind],
                        "BOXID": req.body.data.box_id
                    }, 'detail_goods');
                }
                //await Request_Model.updateGoodsCount(req.body.data.box_id, req.body.data.good_ids.length);
                await Request_Model.updateRequestDetail({
                    GOODS_COUNT: 0,
                    MANAGE_STATUS: 5
                }, {
                    BOXID : req.body.data.box_id
                });
            }
            else {
                var paymentObj = {
                    "STATUS": "PROGRESS" ,
                    "SDETAIL": "SHIPPING" ,
                    "PAY_DESC": "개별물건찾기" ,
                    "COST": req.body.data.cost ,
                    "NOTE": req.body.data.hasOwnProperty('desc') ? req.body.data.desc : ''
                };
                await payment.paymentProcess(req , res ,  user_info[0] , paymentObj);
                var goodIDs = req.body.data.good_ids;
                let find_table_id = await Find_Model.insert({
                        "NOTE" : goodIDs.length + "개 개별 물건 찾기" ,
                        "TRANS_NUMBER" : "" ,
                        "BOXID" : req.body.data.box_id ,
                        "PRICE" : req.body.data.cost ,
                        "GOODS_COUNT" : goodIDs.length ,
                        "ADDR" : req.body.data.addr1 + " " + req.body.data.addr2 + " " + req.body.data.detail_addr
                    });
                for(var ind = 0; ind < goodIDs.length; ind++) {
                    await Request_Model.updateRequestDetail({
                        "STATUS": 3,
                        "RESET_YN": "Y",
                        "FIND_TABLE_ID": find_table_id
                    }, {
                        "GOODSID": goodIDs[ind],
                        "BOXID": req.body.data.box_id
                    }, 'detail_goods');
                }
                await Request_Model.updateGoodsCount(req.body.data.box_id, goodIDs.length);
            }
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    } catch(err) {
        console.log(err);
        response['status'] = 'fail';
        if(err.hasOwnProperty(errMsg))
            response['errMsg'] = err.errMsg;
        else
            response['errMsg'] = message.apiFailed;
        res.json(response);
    }
}
exports.removeAccount = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }

        var userid = user_info[0].ID;
        User_Model.update({
            "EXIT_YN": "Y"
        }, {
            "ID": userid
        });

        res.json(response);
        return;
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    });
}
