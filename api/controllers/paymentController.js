'use strict';
const { Iamporter, IamporterError } = require('iamporter');
var User_Model = require('../models/users_model')
, Payment_Model = require('../models/payment_model')
, message = require('../config/message')
, settings = require('../config/settings')
, response = {
    "status":"success" ,
    "errMsg": ""
}
, global = require('../config/global');
var mysqlConnect = null , iamporter = null;
exports.setConnect = function(connect , paymentConnect) {
    mysqlConnect = connect;
    iamporter = paymentConnect;
    Payment_Model.setConnect(mysqlConnect);
}
var request_process2 = function(req , res , billing_key , user_info , trans_num , paymentObj , imp_uid) {
    return new Promise((resolve , reject) => {
        Payment_Model.insert({
            "USER_ID": user_info['ID'] ,
            "STATUS": paymentObj.STATUS ,
            "SDETAIL": paymentObj.SDETAIL ,
            "PAY_DESC": paymentObj.PAY_DESC ,
            "COST": paymentObj.COST ,
            "TRANS_NUM": trans_num ,
            "NOTE": paymentObj.NOTE ,
            "MERCHANT_ID": imp_uid
        })
        .then((result) => {
                resolve(trans_num);
        })
        .catch((err) => {
             var   obj = {
                    "errMsg" : message.paymentStoreFailed
             }
             reject(obj);
        });
    });
}
var request_process1 = function(req , res , billing_key , user_info , paymentObj) {
    return new Promise((resolve , reject) => {
        Payment_Model.createNewPaymentOrderid()
        .then((trans_num) => {
            /* server update
            iamporter.paySubscription({
                'customer_uid': billing_key,
                'merchant_uid': trans_num,
                'amount': req.body.data.cost
            }).then(result => {
                if(result.status != 200) {
                    var  obj = {
                        "errMsg" : result.message
                    }
                    reject(obj);
                }
                else { */
                    request_process2(req, res, billing_key, user_info, trans_num, paymentObj, /* server update result.data.imp_uid */'imps_531588093595')
                    .then((result)=> {
                        resolve(result);
                    })
                    .catch((err) => {
                    var  obj = {
                            "errMsg" : err.errMsg
                        }
                        reject(obj);
                    })
               /* }
            }).catch(err => {
                var  obj = {
                    "errMsg" : err.message
                }
                reject(obj);
            }); */
        })
        .catch((err) => {
                console.log(err);
                var obj = {
                    "errMsg" : message.apiFailed
                }
                reject(obj);
        })
    })
}
exports.paymentProcess = function(req , res , user_info , paymentObj) {
        if( user_info['CARD_NUM'] == req.body.data.card_num
            && user_info['CARD_MM'] == req.body.data.card_mm
            && user_info['CARD_YY'] == req.body.data.card_yy
            && user_info['CARD_PWD'] == req.body.data.card_pwd
            && user_info['BIRTH_NO'] == req.body.data.card_birth_no ) {
                return new Promise((resolve , reject) => {
                    request_process1(req , res , user_info['BILLING_KEY'] , user_info , paymentObj)
                    .then((result) => {
                        resolve(result);
                    })
                    .catch((err) => {
                       var  obj = {
                            "errMsg" : err.errMsg
                        }
                        reject(obj);
                    })
                })
            }
            var customer_uid = global.generateCustomerUid(user_info['USERID'] , user_info['ID'] , req.body.data.card_num);
            return new Promise((resolve , reject) => {
                /*
                server update
                iamporter.createSubscription({
                    'customer_uid': customer_uid,
                    'card_number': req.body.data.card_num,
                    'expiry': "20" + req.body.data.card_yy + '-' + req.body.data.card_mm,
                    'birth': req.body.data.card_birth_no,
                    'pwd_2digit': req.body.data.card_pwd
                }).then(result => {
                    if(result.status == 200) { */
                        User_Model.update({
                            "CARD_NUM" : req.body.data.card_num , "CARD_MM" : req.body.data.card_mm,
                            "CARD_YY" : req.body.data.card_yy , "CARD_PWD" : req.body.data.card_pwd,
                            "BIRTH_NO" : req.body.data.card_birth_no , "BILLING_KEY" : customer_uid
                        } , { "API_TOKEN" : req.body.data.api_token })
                        .then((result) => {
                            // success
                            request_process1(req , res , customer_uid , user_info , paymentObj)
                            .then((result) => {
                                resolve(result);
                            })
                            .catch((err) => {
                                var obj = {
                                    "errMsg" : err.errMsg
                                }
                                reject(obj);
                            })

                        })
                        .catch((err) => {
				                console.log(err);
                                var obj = {
                                    "errMsg" : message.apiFailed
                                }
                                reject(obj);
                        })
                    /*
                    server update
                    }
                    else {
                            var obj = {
                                "errMsg" : result.message
                            }
                            reject(obj);
                    }
                }).catch(err => {
                        var obj = {
                            "errMsg" : err.message
                        }
                        reject(obj);
                }); */
            });
}
var refund_method = function(req , res , user_info , paymentObj) {
    return new Promise((resolve , reject) => {
        /*console.log({
            'imp_uid': settings.merchant_id,
            'reason': '환불',
            'refund_holder': user_info['DEPOSIT_OWNER_NAME'],
            'refund_bank': user_info['BANK_ID'],
            'refund_account': user_info['ACCOUNT_NUMBER']
        });*/
        /* iamporter.cancel({
            'imp_uid': paymentObj.IMP_UID,
            'reason': '환불',
            'refund_holder': user_info['DEPOSIT_OWNER_NAME'],
            'refund_bank': user_info['BANK_ID'],
            'refund_account': user_info['ACCOUNT_NUMBER']
        })
        .then((result) => {
            Payment_Model.createNewPaymentOrderid()
            .then((trans_num) => {
                Payment_Model.insert({
                    "USER_ID": user_info['ID'] ,
                    "STATUS": paymentObj['STATUS'] ,
                    "SDETAIL": paymentObj['SDETAIL'] ,
                    "PAY_DESC": paymentObj['PAY_DESC'] ,
                    "COST": paymentObj['COST'] ,
                    "TRANS_NUM": trans_num ,
                    "NOTE": ""
                })
                .then((result) => {
                    resolve(trans_num);
                })
                .catch((err) => {
                     reject(err);
                });
            })
            .catch((err)=> {
                reject(err);
            });
        })
        .catch((err) => {
            reject(err);
        }) */
        Payment_Model.createNewPaymentOrderid()
        .then((trans_num) => {
            Payment_Model.insert({
                "USER_ID": user_info['ID'] ,
                "STATUS": paymentObj['STATUS'] ,
                "SDETAIL": paymentObj['SDETAIL'] ,
                "PAY_DESC": paymentObj['PAY_DESC'] ,
                "COST": paymentObj['COST'] ,
                "TRANS_NUM": trans_num ,
                "NOTE": paymentObj['NOTE']
            })
            .then((result) => {
                resolve(trans_num);
            })
            .catch((err) => {
                reject(err);
            });
        })
        .catch((err)=> {
            reject(err);
        });
    });
}
exports.refundProcess = function(req , res , user_info , paymentObj) {
    return new Promise((resolve , reject) => {
        /* server update
          iamporter.cancelByMerchantUid(paymentObj.ORDER_ID, {
            'amount': paymentObj['COST'],
            'reason': '환불'
          })
          .then((result) => { */
                refund_method(req , res , user_info , paymentObj)
                .then((result)=>{
                    resolve(result);
                })
                .catch((err)=>{
                    reject(err);
                });
          /* })
          .catch((err) => {
              reject(err);
          }) */
    });
}