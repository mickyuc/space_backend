'use strict';
var message = require('../config/message') ,
response = {
    "status":"success" ,
    "errMsg": ""
}
, Inquiry_Model = require('../models/inquiry_model')
, Estimate_Model = require('../models/estimate_model')
, User_Model = require('../models/users_model')
, Faq_Model = require('../models/faq_model')
, Notice_Model = require('../models/notice_model')
, settings = require('../config/settings');
var mysqlConnect = null , logger = null;
exports.setDatabaseConnect = function(connect , logger_param) {
    mysqlConnect = connect;
    logger = logger_param;
    Inquiry_Model.setConnect(mysqlConnect);
    Estimate_Model.setConnect(mysqlConnect);
    User_Model.setConnect(mysqlConnect);
    Faq_Model.setConnect(mysqlConnect);
    Notice_Model.setConnect(mysqlConnect);
}
exports.create_estimate = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
    || req.body.data.contents == '' || req.body.data.contents == undefined
    || req.body.data.name == '' || req.body.data.name == undefined
    || req.body.data.contact == '' || req.body.data.contact == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((result) => {
        if(result.length == 1) {
            Estimate_Model.insert({
                "NAME": req.body.data.name ,
                "CONTACT": req.body.data.contact ,
                "QUERY": req.body.data.contents ,
                "USER_ID": result[0]['ID']
            })
            .then((ret) => {
                response['status'] = 'success';
                res.json(response);
            })
            .catch((err) => {
                console.log(err);
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                          response['err'] = err;
                res.json(response);
            })
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.create_inquiry = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
    || req.body.data.title == '' || req.body.data.title == undefined
    || req.body.data.contents == '' || req.body.data.contents == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((result) => {
        if(result.length == 1) {
            Inquiry_Model.insert({
                "CATEGORY": req.body.data.title ,
                "QUERY": req.body.data.contents ,
                "USER_ID": result[0]['ID']
            })
            .then((ret) => {
                response['status'] = 'success';
                res.json(response);
            })
            .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            })
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.get_inquiry_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((result) => {
        if(result.length == 1) {
            Inquiry_Model.getInquiryList({"USER_ID" : result[0]['ID']})
            .then((ret) => {
                response['status'] = 'success';
                response['data'] = [];
                for(let i = 0; i < ret.length; i ++) {
                    response['data'][i] = {
                        "title" : ret[i]['CATEGORY'] ,
                        "inquiry_time" : ret[i]['UPDTIME'] , 
                        "contents" : ret[i]['QUERY'] , 
                        "answer" : ret[i]['ANSWER'] ,
                        "answer_time" : ret[i]['AWRTIME'],
                        "status" : settings.inquiry_status[ret[i]['STATUS']]
                    };
                }
                res.json(response);
            })
            .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            })
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.get_faq_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    Faq_Model.getFaqList()
    .then((result) => {
        response['status'] = 'success';
        response['data'] = {};
        response['data'].faq_list = [];
        for(let i = 0; i < result.length; i ++) {
            response['data']['faq_list'][i] = {
                "category" : result[i]['CATEGORY'] ,
                "title" : result[i]['TITLE'] ,
                "contents" : result[i]['CONTENTS'] ,
                "faq_time" : result[i]['UPDTIME']
            };
        }
        response['data'].category = [];
        response['data'].category = settings.faq_category;
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.get_notice_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    Notice_Model.getNoticeList()
    .then((result) => {
        response['status'] = 'success';
        response['data'] = [];
        for(let i = 0; i < result.length; i ++) {
            response['data'][i] = {
                'notice_time': result[i]['UPDTIME'] ,
                'title': result[i]['TITLE'] ,
                'contents': result[i]['CONTENTS']
            };
        }
        Notice_Model.updateRead(req.body.data.uniqueID);
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.get_estimate_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((result) => {
        if(result.length == 1) {
            Estimate_Model.getEstimateList({"USER_ID" : result[0]['ID']})
            .then((ret) => {
                response['status'] = 'success';
                response['data'] = [];
                for(let i = 0; i < ret.length; i ++) {
                    response['data'][i] = {
                        "name" : ret[i]['NAME'] ,
                        "contact" : ret[i]['CONTACT'] , 
                        "query" : ret[i]['QUERY'] , 
                        "answer" : ret[i]['ANSWER'],
                        "estimate_time" : ret[i]['REGTIME'],
                        'ID' : ret[i]['ID']
                    };
                }
                res.json(response);
            })
            .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            })
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.get_estimate_info = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
    || req.body.data.ID == '' || req.body.data.ID == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    
    Estimate_Model.getEstimateInfo({"ID" : req.body.data.ID})
    .then((ret) => {
        response['status'] = 'success';
        response['data'] = [];
        for(let i = 0; i < ret.length; i ++) {
            response['data'][i] = {
                "name" : ret[i]['NAME'] ,
                "contact" : ret[i]['CONTACT'] , 
                "query" : ret[i]['QUERY'] , 
                "answer" : ret[i]['ANSWER'],
                "estimate_time" : ret[i]['REGTIME'],
                'ID' : ret[i]['ID']
            };
        }
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
        
}