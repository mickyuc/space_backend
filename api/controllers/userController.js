'use strict';
var User_Model = require('../models/users_model')
, axios = require('axios') 
, Verify_Model = require('../models/verify_model')
, Coupon_Model = require('../models/coupon_model')
, message = require('../config/message')
, md5 = require('md5')
, settings = require('../config/settings')
, response = {
    "status":"success" ,
    "errMsg": ""
}
, global = require('../config/global');
const { IamporterError } = require('iamporter');

var mysqlConnect = null , iamporter = null , logger = null;
exports.setDatabaseConnect = function(connect , paymentConnect , logger_param) {
    mysqlConnect = connect;
    iamporter = paymentConnect;
    logger = logger_param;
    User_Model.setConnect(mysqlConnect);
    Verify_Model.setConnect(mysqlConnect);
    Coupon_Model.setConnect(mysqlConnect);
}
exports.test = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    axios.post(
        'https://space.asfd.online:3000/user/test',
        {
		"data": { 
            		}
        },
    )
    .then((res) => {
        res.json(response);
    })
    .catch((err) => {
        response["status"] = "failed";
        res.json(response);
    })
}
exports.signin = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.email == '' || req.body.data.email == undefined
       || req.body.data.password == '' || req.body.data.password == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);   
        return;
    }
    User_Model.getUserList({"USER_EMAIL" : req.body.data.email, "PWD" : md5(req.body.data.password), "USER_TYPE" : 5})
    .then((result)=>{
        if(result.length == 1) {
            let apiToken = global.generateRandomString();
            response['status'] = 'success';
            response['data'] = {
                "mobile": result[0]['USER_CELL'],
                "addr1": result[0] ['ADDR1'],
                "addr2": result[0] ['ADDR2'],
                "detail_addr": result[0] ['DETAIL_ADDR'],
                "username": result[0]['USERNAME'],
                "userid": result[0]['USERID'],
                "userno": result[0]['ID'],
                "apitoken": apiToken,
                "user_type": result[0]['USER_TYPE'],
                "email": result[0]['USER_EMAIL']
            };
            User_Model.update({"PLAYER_ID": ""} , {"PLAYER_ID" : req.body.data.player_id})
            .then((reesult) => {
                User_Model.update({"API_TOKEN" : apiToken , "PLAYER_ID" : req.body.data.player_id} , {"ID" : result[0]['ID']});
            })
            .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
                return;
            })
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.loginFailed;
        }
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
var registerCoupon = function(userid , req , res , response , type , apiToken = '') {
    Coupon_Model.getSystemCouponList()
    .then((result) => {
        for(let i = 0; i < result.length; i ++) {
            Coupon_Model.insertCoupon({
                "COUPON_ID": result[i]['ID'] ,
                "USER_ID": userid
            });
        }

        var include_player_ids = [];
        include_player_ids.push(req.body.data.player_id);
        var message = {
            app_id: settings.onesignal_app_id,
            "contents": {"en": "회원가입이 완료 되었습니다. 서비스 신청을 통해 보다 여유로운 공간을 만들어보세요." , "kr": "회원가입이 완료 되었습니다. 서비스 신청을 통해 보다 여유로운 공간을 만들어보세요."} ,
            include_player_ids: include_player_ids,
            "headings": {"en": "[회원가입 완료]" , "kr": "[회원가입 완료]"} ,
            "data": {"page": "no_page"}
        }
        global.sendNotification(message);

        if(type == 0) {
            response['status'] = "success";
            response['errMsg'] = "";
            res.json(response);
            return;
        }
        else {
            response['status'] = 'success';
            response['data'] = {
                "mobile": req.body.data.mobile,
                "addr1": '',
                "addr2": '',
                "detail_addr": '',
                "username": '',
                "userid": req.body.data.userid,
                "userno": userid,
                "apitoken": apiToken,
                "user_type": req.body.data.user_type,
                "email": req.body.data.email
            };
            response['errMsg'] = '';
            res.json(response);
            return;
        }
    })
    .catch((err) => {
        console.log(err);
        response['status'] = "fail";
        response['errMsg'] = message.apiFailed;
        res.json(response);
        return;
    })
}
exports.signup = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.email == '' || req.body.data.email == undefined
    || req.body.data.password == '' || req.body.data.password == undefined
    || req.body.data.userid == '' || req.body.data.userid == undefined
    || req.body.data.mobile == '' || req.body.data.mobile == undefined
    || req.body.data.player_id == '' || req.body.data.player_id == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    Promise.all([User_Model.getUserList({"USERID" : req.body.data.userid}) , User_Model.getUserList({"USER_EMAIL" : req.body.data.email}) , User_Model.getUserList({"USER_CELL" : req.body.data.mobile})])
    .then((values) => {
        response['status'] = 'fail';
        if(values[0].length > 0)  {
            response['errMsg'] = message.duplicateId;
            res.json(response);
        }
        else if(values[1].length > 0)  {
            response['errMsg'] = message.duplicateEmail;
            res.json(response);
        }
        else if(values[2].length > 0) {
            response['errMsg'] = message.duplicateMobile;
            res.json(response);
        }
        else {
            // mobile
            //check if this mobile number is verified in `verify_table`
            // if exist update process flag to true in `verify_table`
            Verify_Model.mobile_check(req.body.data.mobile)
            .then((check_result) => {
                User_Model.insert(
                    {"USER_EMAIL" : req.body.data.email ,
                    "USER_CELL" : req.body.data.mobile ,
                    "USERID" : req.body.data.userid ,
                    "PWD" : md5(req.body.data.password) ,
                    "EXIT_YN" : "N" ,
                    "API_TOKEN" :  global.generateRandomString() })
                    .then((result) => {
                        if(result) {
                            registerCoupon(result , req , res , response , 0);
                        }
                        else {
                            response['errMsg'] = message.signupFailed;
                            res.json(response);
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        response['errMsg'] = message.apiFailed;
                        res.json(response);
                    })
            })
            .catch((check_err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.nonVerifiedMobile;
                res.json(response);
                return;
            });
            // else
            /*
            response['status'] = 'fail';
            response['errMsg'] = message.nonVerifiedMobile;
            res.json(response);
            return;
            */
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.update_profile = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
    || req.body.data.userid =='' || req.body.data.userid == undefined
    || req.body.data.email == '' || req.body.data.email == undefined
    || req.body.data.detail_addr == '' || req.body.data.detail_addr == undefined
    || req.body.data.mobile == '' || req.body.data.mobile == undefined) { 
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);   
        return;
    }
    Promise.all([User_Model.getUserList({"USERID" : req.body.data.userid}) , User_Model.getUserList({"USER_EMAIL" : req.body.data.email})])
    .then((values) => {
        response['status'] = 'fail';
        if(values[0].length > 0) 
            if(values[0][0]['API_TOKEN'] != req.body.data.api_token) {
                response['errMsg'] = message.duplicateId;
                res.json(response);
                return;
            }
        if(values[1].length > 0)  {
            if(values[1][0]['API_TOKEN'] != req.body.data.api_token) {
                response['errMsg'] = message.duplicateEmail;
                res.json(response);
                return;
            }
        }
        User_Model.update({
          "USERID" : req.body.data.userid , "USER_EMAIL" : req.body.data.email , "ADDR1" : req.body.data.addr1 ,
          "ADDR2" : req.body.data.addr2 , "DETAIL_ADDR" : req.body.data.detail_addr , "USER_CELL" : req.body.data.mobile
        } , {"API_TOKEN" : req.body.api_token})
        .then((result) => {
            response['status'] = 'success';
            res.json(response);   
        })
        .catch((err) => {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;    
            res.json(response);   
        })
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);   
    })
}
exports.find_password = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.mobile == '' || req.body.data.mobile == undefined
    || req.body.data.email == '' || req.body.data.email == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);   
        return;
    }

    // mobile
    //check if this mobile number is verified in `verify_table`
    // if exist update process flag to true in `verify_table`
    Verify_Model.mobile_check(req.body.data.mobile)
    .then((check_result)=>{
        User_Model.getUserList({"USER_EMAIL": req.body.data.email , "USER_CELL": req.body.data.mobile})
        .then((result) => {
            if(result.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.emailVerifiedFailed;
                res.json(response);
                return;
            }
            response['status'] = 'success';
            response['errMsg'] = '';
            response['data'] = { "api_token": result[0]['API_TOKEN']};
            res.json(response);
            return;
        })
        .catch((err) => {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        })
    })
    .catch((check_error)=>{
        response['status'] = 'fail';
        response['errMsg'] = message.nonVerifiedMobile;
        res.json(response);
        return;
    });
    // else
    /*
    response['status'] = 'fail';
    response['errMsg'] = message.nonVerifiedMobile;
    res.json(response);
    return;
    */
}
exports.change_password = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined 
    || req.body.data.new_password == '' || req.body.data.new_password == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);   
        return;
    }
    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then((result)=>{
        if(result.length == 1) {
            User_Model.update({"PWD" : md5(req.body.data.new_password)} , {"API_TOKEN" : req.body.data.api_token})
            .then((result) => {
                response['status'] = 'success';
                res.json(response);
            })
            .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.pwdchangeFailed;
                res.json(response);
            })
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.register_address = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
        || req.body.data.addr1 == '' || req.body.data.addr1 == undefined
        || req.body.data.addr2 == '' || req.body.data.addr2 == undefined
        || req.body.data.detail_addr == '' || req.body.data.detail_addr == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.update({
        "ADDR1" : req.body.data.addr1 , "ADDR2" : req.body.data.addr2,
        "DETAIL_ADDR" : req.body.data.detail_addr
    } , { "API_TOKEN" : req.body.data.api_token })
    .then((result) => {
        response['status'] = 'success';
        response['errMsg'] = '';
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.log_out = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.update({
        "PLAYER_ID": ""
    } , {
        "API_TOKEN": req.body.data.api_token
    })
    .then((result) => {
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

//알리고 이용
exports.sendcode = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.mobile == '' || req.body.data.mobile == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    let verifyNumber = global.generateVerifyNumber();
    
    global.send(req.body.data.mobile , verifyNumber)
    .then((return_data) => {
       if(return_data.result_code != 1) {
            response['status'] = 'fail';
            response['errMsg'] = return_data.message;
            res.json(response);
            return;
        }
        Verify_Model.insert({
            "MOBILE" : req.body.data.mobile ,
            "VERIFY_NUMBER" : verifyNumber ,
            "STATUS" : 0
        })
        .then((result) => {
            response['status'] = 'success';
            response['errMsg'] = '';
            res.json(response);
        })
        .catch((err) => {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        })
    })
    .catch((error_data) => {
       response['status']  = 'fail';
       response['errMsg'] = message.sendcodeFailed;
       res.json(response);
    })

    //else 
    /*response['status'] = 'success';
    response['errMsg'] = '';
    res.json(response);*/
}
exports.verifycode = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.mobile == '' || req.body.data.mobile == undefined
    || req.body.data.verify_code == '' || req.body.data.verify_code == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    // check (req.body.data.mobile  , req.body.data.verify_code) is exist  in `verify_table`
    /*
    not exist
    response['status'] = 'fail';
    response['errMsg'] = message.verifyFailed;
    res.json(response);
    return;
    else
    */
   Verify_Model.getInfo({
       "MOBILE" : req.body.data.mobile ,
       "VERIFY_NUMBER" : req.body.data.verify_code ,
       "STATUS" : 0
   })
   .then((result) => {
        if(result.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.verifyFailed;
            res.json(response);
            return;
        }
        else {
            Verify_Model.update({
                "STATUS" : 1
            } , {
                "MOBILE" : req.body.data.mobile ,  "VERIFY_NUMBER" : req.body.data.verify_code ,  "STATUS" : 0
            })
            .then((update_result) => {
                response['status'] = 'success';
                response['errMsg'] = '';
                res.json(response);
            })
            .catch((update_err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            })
        }
   })
   .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
   })

   //else
    /* response['status'] = 'success';
    response['errMsg'] = '';
    res.json(response); */
}

exports.check_sns = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };

    if(req.body.data.social_id == '' || req.body.data.social_id == undefined
    || req.body.data.user_type == '' || req.body.data.user_type == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    User_Model.getUserList({"UNIQUE_ID": req.body.data.social_id , "USER_TYPE": req.body.data.user_type})
    .then((result) => {
        if(result.length >= 1) {
            response['status'] = 'success';
            response['is_login'] = true;
            response['errMsg'] = '';
            let apiToken = global.generateRandomString();
            response['data'] = {
                "mobile": result[0]['USER_CELL'],
                "addr1": result[0] ['ADDR1'],
                "addr2": result[0] ['ADDR2'],
                "detail_addr": result[0] ['DETAIL_ADDR'],
                "username": result[0]['USERNAME'],
                "userid": result[0]['USERID'],
                "userno": result[0]['ID'],
                "apitoken": apiToken ,
                "user_type": result[0]['USER_TYPE'],
                "email": result[0]['USER_EMAIL']
            };
            User_Model.update({"PLAYER_ID": ""} , {"PLAYER_ID" : req.body.data.player_id})
            .then((reesult) => {
                User_Model.update({"API_TOKEN" : apiToken , "PLAYER_ID" : req.body.data.player_id} , {"ID" : result[0]['ID']});
            })
            .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
                return;
            })
        }
        else {
            response['status'] = 'success';
            response['is_login'] = false;
            response['errMsg'] = '';
        }
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.sns_signup = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };

    if(req.body.data.social_id == '' || req.body.data.social_id == undefined
    || req.body.data.user_type == '' || req.body.data.user_type == undefined
    || req.body.data.email == '' || req.body.data.email == undefined
    || req.body.data.userid == '' || req.body.data.userid == undefined
    || req.body.data.mobile == '' || req.body.data.mobile == undefined
    )
    {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    Promise.all([User_Model.getUserList({"USERID" : req.body.data.userid}) , User_Model.getUserList({"USER_EMAIL" : req.body.data.email}) , User_Model.getUserList({"USER_CELL" : req.body.data.mobile})])
    .then((values) => {
        response['status'] = 'fail';
        if(values[0].length > 0)  {
            response['errMsg'] = message.duplicateId;
            res.json(response);
        }
        else if(values[1].length > 0)  {
            response['errMsg'] = message.duplicateEmail;
            res.json(response);
        }
        else if(values[2].length > 0) {
            response['errMsg'] = message.duplicateMobile;
            res.json(response);
        }
        else {
            // mobile
            //check if this mobile number is verified in `verify_table`
            // if exist update process flag to true in `verify_table`
            Verify_Model.mobile_check(req.body.data.mobile)
            .then((check_result)=>{
                User_Model.update({"PLAYER_ID": ""} , {"PLAYER_ID" : req.body.data.player_id})
                .then((reesult) => {
                    let apiToken = global.generateRandomString();
                    User_Model.insert(
                        {"USER_EMAIL" : req.body.data.email ,
                        "USER_CELL" : req.body.data.mobile ,
                        "USERID" : req.body.data.userid ,
                        "USER_TYPE" :  req.body.data.user_type ,
                        "UNIQUE_ID" : req.body.data.social_id ,
                        "EXIT_YN" : "N" ,
                        "API_TOKEN" :  apiToken ,
                        "PLAYER_ID" : req.body.data.player_id })
                        .then((result) => {
                            if(result) {
                                registerCoupon(result , req , res , response , 1 , apiToken);
                            }
                            else{
                                response['status'] = 'fail';
                                response['errMsg'] = message.signupFailed;
                                res.json(response);
                            }
                        })
                        .catch((err) => {
                            response['errMsg'] = message.apiFailed;
                            res.json(response);
                        })
                })
                .catch((err) => {
                    response['status'] = 'fail';
                    response['errMsg'] = message.apiFailed;
                    res.json(response);
                    return;
                })
            })
            .catch((check_error)=>{
                response['status'] = 'fail';
                response['errMsg'] = message.nonVerifiedMobile;
                res.json(response);
                return;
            });
        }
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.register_playerid = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    User_Model.update({
        "PLAYER_ID": req.body.data.player_id
    } , {
        "ID" : req.body.data.user_info['ID']
    })
    .then((reuslt) => {
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    });
}
exports.get_notice = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.deviceID == '' || req.body.data.deviceID == null) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getNotice({"DEVICE_ID": req.body.data.deviceID })
    .then((result) => {
        response['status'] = 'success';
        response['errMsg'] = '';
        if(result.length > 0)
            response['data'] = result[0];
        else
            response['data'] = null;
        console.log(result);
        res.json(response);
    })
    .catch((err) => {
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}