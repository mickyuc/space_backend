'use strict';
var User_Model = require('../models/users_model')
, Coupon_Model = require('../models/coupon_model')
, Payment_Model = require('../models/payment_model')
, md5 = require('md5')
, message = require('../config/message')
, settings = require('../config/settings')
, response = {
    "status":"success" ,
    "errMsg": ""
}
, global = require('../config/global');
var mysqlConnect = null , iamporter = null , logger = null;
exports.setDatabaseConnect = function(connect , paymentConnect , logger_param) {
    mysqlConnect = connect;
    iamporter = paymentConnect;
    logger = logger_param;
    User_Model.setConnect(mysqlConnect);
    Coupon_Model.setConnect(mysqlConnect);
    Payment_Model.setConnect(mysqlConnect);
}
var update_userinfo_process1 = function(req , res , user_info , response) {
    if(user_info[0]['USERID'] != req.body.data.userid) {
        User_Model.getUserList({"USERID":req.body.data.userid})
        .then((userid_check_info) => {
            if(userid_check_info.length > 0) {
                response['status'] = 'fail';
                response['errMsg'] = message.duplicateId;
                res.json(response);
                return;
            }
            update_userinfo_process2(req , res , user_info , response);
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        });
    }
    else
        update_userinfo_process2(req , res , user_info , response);
}
var update_userinfo_process2 = function(req , res , user_info , response) {
    if(user_info[0]['USER_EMAIL'] != req.body.data.email) {
        User_Model.getUserList({"USER_EMAIL":req.body.data.email})
        .then((email_check_info) => {
            if(email_check_info.length > 0) {
                response['status'] = 'fail';
                response['errMsg'] = message.duplicateEmail;
                res.json(response);
                return;
            }
            update_userinfo_process3(req , res , user_info , response);
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        });
    }
    else
        update_userinfo_process3(req , res , user_info , response);
}

var update_userinfo_process3 = function(req , res , user_info , response) {
    if(user_info[0]['USER_CELL'] != req.body.data.mobile) {
        //check if this mobile number is verified in `verify_table`
        // if exist update process flag to true in `verify_table`
        Verify_Model.mobile_check(req.body.data.mobile)
        .then((check_result)=>{
            User_Model.update({
                "USERID": req.body.data.userid,
                "USER_EMAIL": req.body.data.email,
                "DETAIL_ADDR": req.body.data.detail_addr,
                "ADDR1": req.body.data.addr1,
                "ADDR2": req.body.data.addr2,
                "USER_CELL": req.body.data.mobile
            } , {"API_TOKEN" : req.body.data.api_token})
            .then((result) => {
                response['status'] = 'success';
                response['errMsg'] = '';
                res.json(response);
            })
            .catch((err) => {
                logger.error(err);
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            });
        })
        .catch((check_error)=>{
            logger.error(check_error);
            response['status'] = 'fail';
            response['errMsg'] = message.nonVerifiedMobile;
            res.json(response);
            return;
        });
        // else
        /*
        response['status'] = 'fail';
        response['errMsg'] = message.nonVerifiedMobile;
        res.json(response);
        return;
        */
    }
    else {
        User_Model.update({
            "USERID": req.body.data.userid,
            "USER_EMAIL": req.body.data.email,
            "DETAIL_ADDR": req.body.data.detail_addr,
            "ADDR1": req.body.data.addr1,
            "ADDR2": req.body.data.addr2,
            "USER_CELL": req.body.data.mobile
        } , {"API_TOKEN" : req.body.data.api_token})
        .then((result) => {
            response['status'] = 'success';
            response['errMsg'] = '';
            res.json(response);
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        });
    }
}

exports.update_userinfo = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
    || req.body.data.userid == '' || req.body.data.userid == undefined
    || req.body.data.email == '' || req.body.data.email == undefined
    || req.body.data.detail_addr == '' || req.body.data.detail_addr == undefined
    || req.body.data.addr1 == '' || req.body.data.addr1 == undefined
    || req.body.data.addr2 == '' || req.body.data.addr2 == undefined
    || req.body.data.mobile == '' || req.body.data.mobile == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
            if(user_info.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.noUser;
                res.json(response);
                return;
            }
            update_userinfo_process1(req , res , user_info , response)
    })
    .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
    });
}
exports.modify_password = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
    || req.body.data.cur_password == '' || req.body.data.cur_password == undefined
    || req.body.data.new_password == '' || req.body.data.new_password == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }
        if(user_info[0]['PWD'] != md5(req.body.data.cur_password)) {
            response['status'] = 'fail';
            response['errMsg'] = message.curpasswordWrong;
            res.json(response);
            return;
        }
        User_Model.update({
            "PWD" : md5(req.body.data.new_password)
        } , {"API_TOKEN" : req.body.data.api_token})
        .then((result) => {
            response['status'] = 'success';
            response['errMsg'] = '';
            res.json(response);
            return;
        })
        .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.pwdchangeFailed;
            res.json(response);
            return;
        })
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}
exports.get_coupon_list = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
            if(user_info.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.noUser;
                res.json(response);
                return;
            }
            //responseHomeData(res , response , user_info[0]['ID']);
            Coupon_Model.getCouponList(user_info[0]['ID'])
            .then((result) => {
                response['status'] = 'success';
                response['data'] = {};
                response['data']['coupon_list'] = [];
                for(let i = 0; i < result.length; i ++) {
                    response['data']['coupon_list'][i] = {};
                    response['data']['coupon_list'][i]['coupon_id'] = result[i]['ID'];
                    response['data']['coupon_list'][i]['name'] = result[i]['NAME'];
                    response['data']['coupon_list'][i]['discount'] = result[i]['DISCOUNT'];
                    response['data']['coupon_list'][i]['use_date'] = result[i]['USEDATE'];
                    response['data']['coupon_list'][i]['description'] = result[i]['DESCRIPTION'];
                    response['data']['coupon_list'][i]['use_months'] = result[i]['USE_DATE'];
                    response['data']['coupon_list'][i]['use_flag'] = true;
                    response['data']['coupon_list'][i]['used'] = result[i]['USED'];
                    response['data']['coupon_list'][i]['issued'] = result[i]['ISSUED'];
                }
                res.json(response);
                return;
            })
            .catch((err) => {
                logger.error(err);
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            })
    })
    .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
    });
}

exports.update_cardinfo = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
        || req.body.data.card_num == '' || req.body.data.card_num == undefined
        || req.body.data.card_mm == '' || req.body.data.card_mm == undefined
        || req.body.data.card_yy == '' || req.body.data.card_yy == undefined
        || req.body.data.card_birth_no == '' || req.body.data.card_birth_no == undefined
        || req.body.data.card_pwd == '' || req.body.data.card_pwd == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
    .then((result) => {
        if(result.length == 1) {
            if( result[0]['CARD_NUM'] == req.body.data.card_num
            && result[0]['CARD_MM'] == req.body.data.card_mm
            && result[0]['CARD_YY'] == req.body.data.card_yy
            && result[0]['CARD_PWD'] == req.body.data.card_pwd
            && result[0]['BIRTH_NO'] == req.body.data.card_birth_no ) {
                response['status'] = 'fail';
                response['errMsg'] = message.alreadyRegisteredMessage;
                res.json(response);
                return;
            }
            var customer_uid = global.generateCustomerUid(result[0]['USERID'] , result[0]['ID'] , req.body.data.card_num);
            iamporter.createSubscription({
                    'customer_uid': customer_uid,
                    'card_number': req.body.data.card_num,
                    'expiry': "20" + req.body.data.card_yy + '-' + req.body.data.card_mm,
                    'birth': req.body.data.card_birth_no,
                    'pwd_2digit': req.body.data.card_pwd
            }).then(result => {
                if(result.status == 200) {
                    User_Model.update({
                        "CARD_NUM" : req.body.data.card_num , "CARD_MM" : req.body.data.card_mm,
                        "CARD_YY" : req.body.data.card_yy , "CARD_PWD" : req.body.data.card_pwd,
                        "BIRTH_NO" : req.body.data.card_birth_no , "BILLING_KEY" : customer_uid
                    } , { "API_TOKEN" : req.body.data.api_token })
                    .then((result) => {
                        response['status'] = 'success';
                        response['errMsg'] = '';
                        res.json(response);
                    })
                    .catch((err) => {
                        logger.error(err);
                        response['status'] = 'fail';
                        response['errMsg'] = message.apiFailed;
                        res.json(response);
                    })
                }
                else {
                    response['status'] = 'fail';
                    response['errMsg'] = result.message;
                    res.json(response);
                }
            }).catch(err => {
                logger.error(err);
                response['status'] = 'fail';
                response['errMsg'] = err.message;
                res.json(response);
            });
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
        }
    })
    .catch((err) => {
        logger.error(err);
        response['status'] = 'fail';
        response['errMsg'] = message.apiFailed;
        res.json(response);
    })
}

exports.get_transaction_history = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
            if(user_info.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.noUser;
                res.json(response);
                return;
            }
            //update_userinfo_process1(req , res , user_info , response)
            Payment_Model.getPaymentLogList({"USER_ID" : user_info[0]['ID']})
            .then((result) => {
                response['status'] = 'success';
                response['errMsg'] = '';
                response['data'] = {};  response['data']['payment_log'] = [];
                for(let i = 0; i < result.length; i ++) {
                    response['data']['payment_log'][i] = {};
                    response['data']['payment_log'][i] = {
                        'payment_id' : result[i]['ID'] ,
                        'payment_time' : result[i]['UPDTIME'] ,
                        'cost' : result[i]['COST'] ,
                        'desc' : result[i]['NOTE'] ,
                        'payment_type' : result[i]['PAY_DESC']
                    }
                }
                res.json(response);
                return;
            })
            .catch((err) => {
                logger.error(err);
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
            });
    })
    .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
    });
}

exports.get_cardinfo = function(req, res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
            if(user_info.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.noUser;
                res.json(response);
                return;
            }
            response['status'] = 'success';
            response['errMsg'] = '';
            response['data'] = {
                "card_num": user_info[0]['CARD_NUM'] , 
                "card_mm": user_info[0]['CARD_MM'] ,
                "card_yy": user_info[0]['CARD_YY'] ,
                "card_pwd": user_info[0]['CARD_PWD'] ,
                "card_birth_no": user_info[0]['BIRTH_NO']
            }
            res.json(response);
    })
    .catch((err) => {
            logger.error(err);
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
    });
}

exports.get_bank_list = function(req,  res) {
    response = {
        "status":"success" , "errMsg": ""
    };

    response['data'] = {};
    response['data']['bank_list'] = [
        {"name" : "KB국민은행" , "id" : "004"},
        {"name" : "SC제일은행" , "id" : "023"},
        {"name" : "경남은행" , "id" : "039"},
        {"name" : "광주은행" , "id" : "034"},
        {"name" : "기업은행" , "id" : "003"},
        {"name" : "농협" , "id" : "011"},
        {"name" : "대구은행" , "id" : "031"},
        {"name" : "부산은행" , "id" : "032"},
        {"name" : "산업은행" , "id" : "002"},
        {"name" : "새마을금고" , "id" : "045"},
        {"name" : "수협" , "id" : "007"},
        {"name" : "신한은행" , "id" : "088"},
        {"name" : "신협" , "id" : "048"},
        {"name" : "외환은행" , "id" : "005"},
        {"name" : "우리은행" , "id" : "020"},
        {"name" : "우체국" , "id" : "071"},
        {"name" : "전북은행" , "id" : "037"},
        {"name" : "카카오뱅크" , "id" : "090"},
        {"name" : "케이뱅크" , "id" : "089"},
        {"name" : "하나은행" , "id" : "081"},
        {"name" : "한국씨티은행" , "id" : "027"}
    ];

    res.json(response);
}

exports.register_refund_account = async function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };

    if(req.body.data.api_token == '' || req.body.data.api_token == undefined
        || req.body.data.bank_id == '' || req.body.data.bank_id == undefined
        || req.body.data.deposit_owner_name == '' || req.body.data.deposit_owner_name == undefined
        || req.body.data.account_number == '' || req.body.data.account_number == undefined
      ) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }
    try {
     /*   User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
        .then((user_info) => {
                if(user_info.length < 1) {
                    response['status'] = 'fail';
                    response['errMsg'] = message.noUser;
                    res.json(response);
                    return;
                }
                User_Model.update({
                    "BANK_ID" : req.body.data.bank_id,
                    "DEPOSIT_OWNER_NAME" : req.body.data.deposit_owner_name,
                    "ACCOUNT_NUMBER" : req.body.data.account_number
                } , {
                    "ID" : user_info[0]['ID']
                })
                .then((result) => {
                    response['status'] = 'success';
                    response['errMsg'] = '';
                    res.json(response);
                })
                .catch((err) => {
                    response['status'] = 'fail';
                    response['errMsg'] = message.apiFailed;
                    res.json(response);
                })
        })
        .catch((err) => {
                response['status'] = 'fail';
                response['errMsg'] = message.apiFailed;
                res.json(response);
        }); */
        let user_info = await User_Model.getUserList({"API_TOKEN" : req.body.data.api_token});
        if(user_info.length < 1) {
            response['status'] = 'fail';
            response['errMsg'] = message.noUser;
            res.json(response);
            return;
        }

        let ret1 = await global.authenticate();

        if(ret1.data.code != 0) {
            response['status'] = 'fail';
            response['errMsg'] = ret1.data.message;
            res.json(response);
            return;
        }

        let ret2 = await global.checkBankHolder(ret1.data.response.access_token , req.body.data.bank_id , req.body.data.account_number);

        let update_result = await User_Model.update({
            "BANK_ID" : req.body.data.bank_id,
            "DEPOSIT_OWNER_NAME" : req.body.data.deposit_owner_name,
            "ACCOUNT_NUMBER" : req.body.data.account_number
        } , {
            "ID" : user_info[0]['ID']
        });

        if(update_result) {
            response['status'] = 'success';
            response['errMsg'] = '';
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        }
    }
    catch(err) {

        logger.error(err);

        if(err instanceof Error) {
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
        }
        else {
            response['status'] = 'fail';
            response['errMsg'] = err;
            res.json(response);
        }
    }
}

exports.get_refund_info = function(req , res) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if(req.body.data.api_token == '' || req.body.data.api_token == undefined) {
        response['status'] = 'fail';
        response['errMsg'] = message.invalidParameter;
        res.json(response);
        return;
    }

    User_Model.getUserList({"API_TOKEN":req.body.data.api_token})
    .then((user_info) => {
            if(user_info.length < 1) {
                response['status'] = 'fail';
                response['errMsg'] = message.noUser;
                res.json(response);
                return;
            }
            response['status'] = 'success';
            response['errMsg'] = '';
            response['data'] = {
                "bank_id": user_info[0]['BANK_ID'] ,
                "deposit_owner_name": user_info[0]['DEPOSIT_OWNER_NAME'] ,
                "account_number": user_info[0]['ACCOUNT_NUMBER']
            };
            res.json(response);
            return;
    })
    .catch((err) => {
        
            logger.error(err);

            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
    });
}