var global = require('../config/global') 
, mysqlConnect
, table = 'users';

exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getUserList = function(filter = {}) {
    var sql = "select * from " + table + " where DEL_YN='N' and EXIT_YN = 'N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.update = function(data = {} , filter = {}) {
    var sql = "update " + table + " set UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data)) 
        sql += " , " + key + " = '" + value + "' ";
    sql += " where 1=1 " ;
    for(let[key , value] of Object.entries(filter)) 
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else 
                resolve(true);
        });
    })
}
exports.insert = function(data = {}) {
    var sql = "insert into " + table + " set DEL_YN = 'N' , "
    + " REGTIME = " + global.getCurrentTime() + " , "
    + " UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data)) 
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(result.insertId);
        });
    })
}
exports.getNotice = function(filter = {}) {
    var sql = "select * from notice_read where ";
    for(let[key , value] of Object.entries(filter))
        sql += " " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}