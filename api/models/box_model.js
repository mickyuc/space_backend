var mysqlConnect
, table = 'box'
, global = require('../config/global');

exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getBoxList = function(filter = {}) {
    var sql = "select box.* , price_table.PRICE from " + table + " \
    left join price_table \
    on price_table.BOX_ID = box.ID \
    where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}

exports.getBasicPrice = function() {
    var sql = "select * from price_table \
    where TYPE=0 ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['PRICE']);
            }
        });
    })
}

exports.createBoxId= function() {
    var timestamp_value = global.getCurrentDateTimeStamp();
    var sql = "SELECT COUNT(A.ID) AS CNT " +
    "FROM request_detail A " +
    "LEFT JOIN request B " +
    "ON A.`REQUEST_ID` = B.`ID` " +
    "WHERE A.DEL_YN = 'N' " +
    "AND B.`UPDTIME` >= " + timestamp_value;
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['CNT'] + 1);
                //resolve('B' + global.getTodayString() + String(result[0]['CNT'] + 1).padStart(3, '0'));
            }
        });
    })
}