var mysqlConnect
, table = 'banner';

exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getBannerList = function(filter = {}) {
    var sql = "select * from " + table + " where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}