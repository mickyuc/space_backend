var global = require('../config/global') 
, mysqlConnect
, table = 'find_table';

exports.setConnect = function(connect) {
    mysqlConnect = connect;
}

exports.insert = function(data = {}) {
    var sql = "insert into " + table + " set  REGTIME = " + global.getCurrentTime() + " , "
    + " UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(result.insertId);
        });
    })
}