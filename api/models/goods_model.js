var global = require('../config/global') 
, mysqlConnect
, table = 'detail_goods';
exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
// This is for app to show even delivery complete status
exports.getGoodsList = function(userid , boxid) {
    var sql = "select  A.*  " +
    " , B.`START_DATE` " +
    " from detail_goods A " +
    " LEFT JOIN request_detail B " +
    " ON B.`BOXID`= A.`BOXID` " +
    " where A.DEL_YN = 'N' " +
    "AND ( A.`STATUS` = 1 OR A.`STATUS` = 3 ) " +
    "AND B.`USER_ID`  =  " + userid +
    " AND B.`BOXID` = '" + boxid + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
// This is for app to show only save products
exports.getGoodsListOnly = function(userid , boxid) {
    var sql = "select  A.*  " +
    " , B.`START_DATE` " +
    " from detail_goods A " +
    " LEFT JOIN request_detail B " +
    " ON B.`BOXID`= A.`BOXID` " +
    " where A.DEL_YN = 'N' " +
    "AND ( A.`STATUS` = 1 ) " +
    "AND B.`USER_ID`  =  " + userid +
    " AND B.`BOXID` = '" + boxid + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}