'use strict';
const { Iamporter , IamporterError } = require('iamporter');
var mysql = require('mysql');
var settings = require('../config/settings') , connect;
const iamporter = new Iamporter({
    apiKey: settings.api_key,
    secret: settings.api_secret
});
/*function handleDisconnect() 
{
    connect = mysql.createConnection(settings.mysql);
    connect.connect(function(err) {
        if(err) {
            console.log('error when connecting to db:' , err);
            setTimeout(handleDisconnect , 2000);
        }
    });
    connect.on('error' , function(err) {
        console.log('db error' , err);
        if(err.code == 'PROTOCOL_CONNECTION_LOST')
            handleDisconnect();
        else
            throw err;
    });
}
handleDisconnect(); */

connect  = mysql.createPool(settings.mysql);

//-
//- Establish a new connection
//-

connect.getConnection(function(err){
    if(err) {
        console.log("\n\t *** Cannot establish a connection with the database. ***");
        handleDisconnect();
    }else {
        console.log("\n\t *** New connection established with the database. ***")
    }
});
//-
//- Reconnection function
//-
function handleDisconnect(){
    console.log("\n New connection tentative...");

    //- Create a new one
    connect = mysql.createPool(settings.mysql);

    //- Try to reconnect
    connect.getConnection(function(err){
        if(err) {
            //- Try to connect every 2 seconds.
            setTimeout(handleDisconnect, 2000);
        }else {
            console.log("\n\t *** New connection established with the database. ***")
        }
    });
}

//-
//- Error listener
//-
connect.on('error', function(err) {
    //-
    //- The server close the connection.
    //-
    if(err.code === "PROTOCOL_CONNECTION_LOST"){    
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        return handleDisconnect();
    }

    else if(err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT"){
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        return handleDisconnect();
    }

    else if(err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR"){
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        return handleDisconnect();
    }

    else if(err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE"){
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
    }

    else{
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        return handleDisconnect();
    }

}); 

module.exports = {
    connect , iamporter
}