var global = require('../config/global') 
, mysqlConnect
, table = 'payment_log';

exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getPaymentLogList = function(filter = {}) {
    var sql = "select * from " + table + " where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    sql += " order by UPDTIME desc";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.update = function(data = {} , filter = {}) {
    var sql = "update " + table + " set UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data)) 
        sql += " , " + key + " = '" + value + "' ";
    sql += " where 1=1 " ;
    for(let[key , value] of Object.entries(filter)) 
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(true);
        });
    })
}
exports.insert = function(data = {}) {
    var sql = "insert into " + table + " set DEL_YN = 'N' , "
    + " REGTIME = " + global.getCurrentTime() + " , "
    + " UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
        //sql += " , " + key + " = \"" + value + "\" ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(true);
        });
    })
}

exports.createNewPaymentOrderid = function() {
    var timestamp_value = global.getCurrentDateTimeStamp();
    var sql = "SELECT COUNT(A.ID) AS CNT " +
    "FROM payment_log A " +
    "WHERE A.DEL_YN = 'N' " +
    "AND A.`UPDTIME` >= " + timestamp_value;
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve( 'TRANS_' + String( global.getCurrentTime() ) );
            }
        });
    })
}
