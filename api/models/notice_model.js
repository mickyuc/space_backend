var mysqlConnect = null
, table = 'notice';
exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getNoticeList = function(filter = {}) {
    var sql = "select * from " + table + " where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    sql += " ORDER BY UPDTIME DESC ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.updateRead = function(deviceID) {
    var sql = "SELECT * FROM notice_read WHERE DEVICE_ID = '"+deviceID + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            var sql1 = '';
            if(result.length > 0){
                sql1 = "update notice_read set IS_READ = 'Y' WHERE DEVICE_ID = '"+deviceID+"'";
            }else{
                sql1 = "INSERT INTO notice_read(DEVICE_ID, IS_READ) VALUES('"+deviceID+"', 'Y')";
            }
            mysqlConnect.query(sql1);
            resolve(true);
        });
    })
}