var mysqlConnect = null
, table = 'coupon'
, global = require('../config/global');
exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getCouponList = function(userid) {
    var sql = "select B.* , A.USE_DATE AS `USEDATE` , A.USED , A.ISSUED " +
    "from coupon_users A " +
    "LEFT JOIN coupon B " +
    "ON A.`COUPON_ID` = B.ID " +
    "where A.`DEL_YN` = 'N' " +
    "and A.`USER_ID` = " + userid;
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.getSystemCouponList = function() {
    var sql = "SELECT * " +
              "FROM coupon " +
              "WHERE DEL_YN = 'N' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err);
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.insertCoupon = function(data = {}) {
    var sql = "insert into coupon_users set DEL_YN = 'N' , "
    + " REGTIME = " + global.getCurrentTime() + " , "
    + " UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data)) 
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(result.insertId);
        });
    })
}

exports.update = function(data = {} , filter = {}) {
    var sql = "update coupon_users set UPDTIME = " + global.getCurrentTime() + " , " + 
    "USE_DATE = " + global.getCurrentTime();
    for(let[key , value] of Object.entries(data)) 
        sql += " , " + key + " = '" + value + "' ";
    sql += " where 1=1 " ;
    for(let[key , value] of Object.entries(filter)) 
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(true);
        });
    })
}