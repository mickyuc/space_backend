var global = require('../config/global')
, mysqlConnect
, table = 'estimate';
exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getEstimateList = function(filter = {}) {
    var sql = "select * from " + table + " where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    sql += ' order by REGTIME DESC ';
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.getEstimateInfo = function(filter = {}) {
    var sql = "select * from " + table + " where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.insert = function(data = {}) {
    var sql = "insert into " + table + " set DEL_YN = 'N' , "
    + " REGTIME = " + global.getCurrentTime() + " , "
    + " UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(true);
        });
    })
}
