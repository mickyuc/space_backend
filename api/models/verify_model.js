var global = require('../config/global') 
, mysqlConnect
, table = 'verify_table';

exports.setConnect = function(connect) {
    mysqlConnect = connect;
}
exports.getInfo = function(filter = {}) {
    var sql = "select * from " + table + " where 1=1 ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.update = function(data = {} , filter = {}) {

    var sql = "update " + table + " set UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
    sql += " where 1=1 " ;
    
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(true);
        });
    })
}
exports.insert = function(data = {}) {
    var sql = "insert into " + table + " SET "
    + " REGTIME = " + global.getCurrentTime() + " , "
    + " UPDTIME = " + global.getCurrentTime() + "  ";
    for(let[key , value] of Object.entries(data)) 
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(result.insertId);
        });
    })
}

exports.mobile_check = function(mobile) {
    var sql = "select A.*  " +
    " from verify_table A " + 
    " where A.MOBILE = '" + mobile + "' " + 
    " AND A.STATUS = 1";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(false)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                if(result.length < 1) {
                    reject(false);
                }
                else {
                    sql = "delete from verify_table where `MOBILE` = '" + mobile + "' and `STATUS` = 1";
                    mysqlConnect.query(sql , function(err, result) {
                        if(err) 
                            reject(false);
                        else
                            resolve(true);
                    })
                }
            }
        })
    })
}