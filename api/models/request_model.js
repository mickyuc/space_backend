var global = require('../config/global')
    , mysqlConnect
    , table = 'request'
    , request_detail_table = 'request_detail';

exports.setConnect = function (connect) {
    mysqlConnect = connect;
}
exports.getMyRequestCount = function (userid) {
    var sql = "SELECT COUNT(ID) AS rcnt " +
        "FROM " + table +
        " WHERE DEL_YN = 'N' " +
        "AND USER_ID = " + userid;
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['rcnt']);
            }
        });
    })
}
exports.getMyStoreBoxCount = function (userid) {
    var sql = "SELECT count(A.ID) as bcnt " +
        "FROM request_detail A " +
        "WHERE A.`DEL_YN` = 'N' " +
        "AND A.`USER_ID` = " + userid +
        " AND A.`MANAGE_STATUS` = 4 AND A.`GOODS_COUNT` > 0 ";
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['bcnt']);
            }
        })
    })
}
exports.getStorageDetailInfo = function(userid, boxid) {
    var sql = "SELECT D.NAME, B.BOXID, B.START_DATE, B.END_DATE, count(C.STATUS) AS total_count, " + 
    "sum(if(C.STATUS = 1, 1, 0)) as STORAGE_COUNT, sum(if(C.STATUS = 2, 1, 0)) as DELIVERY_COUNT, sum(if(C.STATUS = 3, 1, 0)) as FINISH_COUNT FROM request A " +
    "LEFT JOIN request_detail B ON A.ID = B.REQUEST_ID " +
    "LEFT JOIN box D ON D.ID = B.BOX_NO " +
    "LEFT JOIN detail_goods C ON C.BOXID = B.BOXID AND C.DEL_YN = 'N' " + 
    "WHERE A.USER_ID = "+ userid + " AND A.DEL_YN = 'N' AND B.BOXID = '" + boxid + "' AND B.MANAGE_STATUS = 4 GROUP BY B.ID";

    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        })
    })
}
exports.updateRequest = function(data = {} , filter = {}) {
    var sql = "update " + table + " set UPDTIME = " + global.getCurrentTime() + "  ";
    for (let [key, value] of Object.entries(data))
        sql += " , `" + key + "` = '" + value + "' ";
    sql += " where 1=1 ";
    for (let [key, value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";

    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err)
                reject(err)
            else
                resolve(true);
        });
    });
}
exports.updateGoodsCount = function(boxid, count) {
    var sql = "UPDATE " + request_detail_table + " SET UPDTIME = '" + global.getCurrentTime() + "', "
            + "GOODS_COUNT = GOODS_COUNT - " + count + " "
            + "where 1=1 "
            + "and BOXID = '" + boxid + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else 
                resolve(true);
        });
    });
}
exports.updateRequestDetail = function(data={}, filter={}, table='request_detail') {
    var sql = "UPDATE " + table + " SET UPDTIME = '" + global.getCurrentTime() + "' ";
    for(let[key , value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
    sql += " where 1=1 " ;
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";

    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result) {
            if(err)
                reject(err)
            else
                resolve(true);
        });
    });
}
exports.insertRequest = function (data = {}) {
    var sql = "insert into " + table + " set DEL_YN = 'N' , "
        + " REGTIME = " + global.getCurrentTime() + " , "
        + " UPDTIME = " + global.getCurrentTime() + "  ";
    for (let [key, value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err)
                reject(err)
            else {
                resolve(result.insertId);
            }
        });
    })
}
exports.insertRequestDetail = function (data = {}) {
    var sql = "insert into " + request_detail_table + " set DEL_YN = 'N' , "
        + " REGTIME = " + global.getCurrentTime() + " , "
        + " UPDTIME = " + global.getCurrentTime() + "  ";
    for (let [key, value] of Object.entries(data))
        sql += " , " + key + " = '" + value + "' ";
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err)
                reject(err)
            else
                resolve(true);
        });
    })
}

exports.createTransnumber = function () {
    var timestamp_value = global.getCurrentDateTimeStamp();
    var sql = "SELECT COUNT(A.ID) AS CNT " +
        "FROM request A " +
        "WHERE A.DEL_YN = 'N' " +
        "AND A.`UPDTIME` >= " + timestamp_value;
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result, fields) {
            if (err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve('T' + global.getTodayString() + String(result[0]['CNT'] + 1).padStart(3, '0'));
            }
        });
    });
}
exports.getRequestDetailList = function(filter={}) {
    var sql = "select * from " + request_detail_table + " where DEL_YN='N' and GOODS_COUNT > 0";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.getRequestInfo = function (request_id, select = '*') {
    const sql = `
SELECT ${select} FROM ${table}
WHERE id = ${request_id}
LIMIT 1
`
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}

exports.getRequestPrice = function (request_id) {
    const sql = `
SELECT SUM(PRICE) AS total_price
FROM ${request_detail_table}
WHERE REQUEST_ID = ${request_id}
GROUP BY REQUEST_ID
LIMIt 1
`

    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}

exports.getBoxstorageInfo = function (data = {}) {
   /* const sql = `
SELECT
    b.NAME AS box_name,
    COUNT(rd.BOX_NO) AS amount,
    SUM(rd.GOODS_COUNT) AS goods_count,
    rd.BOXID AS box_id,
    rd.BOX_NO AS box_no
FROM
    request_detail AS rd
        LEFT JOIN
    request AS r ON rd.REQUEST_ID = r.ID
        LEFT JOIN
    box AS b ON b.ID = rd.BOX_NO
        LEFT JOIN
    detail_goods AS dg ON dg.BOXID = rd.BOXID AND dg.DEL_YN = 'N'
WHERE
    rd.MANAGE_STATUS = ${data.status} AND r.USER_ID = ${data.userid} AND rd.GOODS_COUNT > 0
GROUP BY rd.BOX_NO, rd.BOXID
`  */
    const sql = `
    SELECT
        b.NAME AS box_name,
        COUNT(rd.BOX_NO) AS amount,
        rd.GOODS_COUNT AS goods_count,
        rd.BOXID AS box_id,
        rd.BOX_NO AS box_no
    FROM
        request_detail AS rd
    LEFT JOIN
        request AS r ON rd.REQUEST_ID = r.ID
    LEFT JOIN
        box AS b ON b.ID = rd.BOX_NO
    WHERE
        rd.MANAGE_STATUS = ${data.status} AND r.USER_ID = ${data.userid} AND rd.GOODS_COUNT > 0
    GROUP BY rd.BOX_NO, rd.BOXID
    `

    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}

exports.getBoxstorageRemainingInfo = function (data = {}) {
    const sql = `
SELECT
    b.NAME AS box_name,
        rd.GOODS_COUNT AS goods_count,
        DATEDIFF(rd.END_DATE, NOW()) AS rest_days,
        rd.BOXID AS box_id,
        rd.BOX_NO AS box_no
FROM
    request AS r
LEFT JOIN request_detail AS rd ON rd.REQUEST_ID = r.ID
LEFT JOIN box AS b ON b.ID = rd.BOX_NO
WHERE
    rd.MANAGE_STATUS = 4 AND r.USER_ID = ${data.userid} AND rd.GOODS_COUNT = 0 AND DATEDIFF(rd.END_DATE, NOW()) > 0
GROUP BY rd.ID
`

    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}


exports.getBoxstorageProgressInfo = function (data = {}) {
    const sql = `
SELECT
	r.ID AS request_id,
	r.SDETAIL AS detail_type,
    b.NAME AS box_name,
    COUNT(rd.BOX_NO) AS amount,
    SUM(rd.GOODS_COUNT) AS goods_count
FROM
    request AS r
        LEFT JOIN
    request_detail AS rd ON rd.REQUEST_ID = r.ID
        LEFT JOIN
    box AS b ON b.ID = rd.BOX_NO
WHERE
    r.STATUS = '${data.status}' AND (r.CONDITION != 'RequestCancel_RA' AND r.CONDITION != 'RequestCancel_RH' AND r.CONDITION != 'Returning_Impropriety') AND r.USER_ID = ${data.userid} AND rd.MANAGE_STATUS != 4 AND rd.MANAGE_STATUS != 3 AND rd.MANAGE_STATUS != 5
GROUP BY r.ID, r.SDETAIL, rd.BOX_NO
`

    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}

exports.getReusableCount = function(userid) {
    var today = global.getDashTodayString();
    var sql = "SELECT COUNT(ID) AS CNT FROM " + request_detail_table
            + " WHERE DEL_YN = 'N' "
            + "AND END_DATE > '" + today + "' "
            + "AND GOODS_COUNT = 0 "
            + "AND MANAGE_STATUS = 4 "
            + "AND USER_ID = " + userid;
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['CNT']);
            }
        });
    });
}

exports.getReusableList = function(userid, limit = 1) {
    var today = global.getDashTodayString();
    var sql = "SELECT ID , START_DATE , END_DATE , STORAGE_MONTHS FROM " + request_detail_table
            + " WHERE DEL_YN = 'N' "
            + "AND END_DATE > '" + today + "' "
            + "AND GOODS_COUNT = 0 "
            + "AND MANAGE_STATUS = 4 "
            + "AND USER_ID = " + userid
            + " LIMIT " + limit;
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    });
}

var getfinalCancelAmount = function(request_id , refund_cost , price) {

    var sql = "SELECT * " +
    "FROM request " +
    "WHERE ID = " + request_id +
    " AND DEL_YN = 'N'";
    
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);

                let trans_number = result[0]['PAYMENT_MERCHANT_ID'];
                var query = "select * from payment_log where TRANS_NUM = '" + trans_number + "'";
                mysqlConnect.query(query , function(err , payment , fields) {
                    if(err)
                        reject(err);
                    else {
                        payment = JSON.stringify(payment);
                        payment = JSON.parse(payment);
                        let cost = payment[0]['COST'];
                        cost = cost - 10000;
                        if(cost <= refund_cost)
                            resolve(0);
                        else if(cost > refund_cost) {
                            cost = cost - refund_cost;
                            if(cost < price)
                                resolve(cost);
                            else
                                resolve(price);
                        }
                    }
                })
            }
        });
    });
}

var getSumRefundCost = function(request_id , price)  {
    var sql = "SELECT SUM(PRICE) AS SUM_PRICE " +
    "FROM request_detail " +
    "WHERE REQUEST_ID = " + request_id +
    " AND MANAGE_STATUS = 3 ";

    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql, function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                let refund_cost = result[0]['SUM_PRICE'] == null ? 0 : result[0]['SUM_PRICE'];
                getfinalCancelAmount(request_id , refund_cost , price)
                .then((cost) => {
                    resolve(cost);
                })
                .catch((err) => {
                    reject(err);
                })
            }
        })
    });
}

exports.getRemainCost = function(boxid) {
    /*var sql = "SELECT * " +
    " FROM request_detail " +
    " WHERE BOXID = '" + boxid + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql, function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                getSumRefundCost(result[0]['REQUEST_ID'] , result[0]['PRICE'])
                .then((cost) => {
                    resolve(cost);
                })
                .catch((err) => {
                    reject(err);
                })
            }
        })
    })*/
    var sql = "SELECT * " +
    " FROM request_detail " +
    " WHERE BOXID = '" + boxid + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql, function(err, result , fields) {
            if(err)
                reject(err);
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['PRICE']);
            }
        })
    })
}
exports.getOrderidFromBox = function(boxid) {
    var sql = "SELECT * " +
    "FROM request " +
    "LEFT JOIN request_detail " +
    "ON request.ID = request_detail.`REQUEST_ID` " +
    "WHERE request_detail.BOXID = '" + boxid + "'";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result[0]['PAYMENT_MERCHANT_ID']);
            }
        })
    })
}

exports.getRequestDetailInfo = function(filter = {}) {
    var sql = "select * from " + request_detail_table + " where DEL_YN='N' ";
    for(let[key , value] of Object.entries(filter))
        sql += " and " + key + " = '" + value + "' ";
    return new Promise((resolve , reject) => {
        mysqlConnect.query(sql , function(err , result , fields) {
            if(err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}
exports.getTimeList = function (reqeust_date) {
    var sql = "SELECT COUNT(*) AS cnt, REQUEST_TIME " +
        "FROM " + table +
        " WHERE SDETAIL = 'ARRANGE' " +
        "AND REQUEST_DATE = '" + reqeust_date + "' GROUP BY REQUEST_TIME";
    return new Promise((resolve, reject) => {
        mysqlConnect.query(sql, function (err, result) {
            console.log(result);
            if (err)
                reject(err)
            else {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                resolve(result);
            }
        });
    })
}