var express = require('express');
const app = express(),
port = process.env.PORT || 3000 ,
bodyParser = require('body-parser') ,
User_Model = require('./api/models/users_model'),
fs = require('fs') ,

space_model = require('./api/models/space_model');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var userRoute = require('./api/routes/userRoutes')
, utilRoute = require('./api/routes/utilRoutes')
, serviceRoute = require('./api/routes/serviceRoutes')
, mypageRoute = require('./api/routes/mypageRoutes.js')
, message = require('./api/config/message');

User_Model.setConnect(space_model.connect);

const log4js = require('log4js');
log4js.configure({
    appenders: { space: { type: 'file', filename: '/var/www/html/space-backend/space.log' } },
    categories: { default: { appenders: ['space'], level: 'error' } }
});
const logger = log4js.getLogger('space');

var privateKey  = fs.readFileSync('/etc/letsencrypt/live/land-space.ku-min.com/privkey.pem', 'utf8');
var certificate = fs.readFileSync('/etc/letsencrypt/live/land-space.ku-min.com/fullchain.pem', 'utf8');
var credentials = {key: privateKey, cert: certificate};

app.use(function (req, res, next) {
    response = {
        "status":"success" , "errMsg": ""
    };
    if( !req.body.data.hasOwnProperty('api_token') || req.body.data.api_token == '' || req.body.data.api_token == undefined  || req.originalUrl.includes("getHomeData") ) {
        next()
    }
    else{
        //req.body.data.new_value = "asdfsadfjskaljdfalk;sdjf;asdklfsdfas";
        User_Model.getUserList({"API_TOKEN" : req.body.data.api_token})
        .then((result)=>{
            if(result.length == 1) {
                req.body.data.user_info = result[0];
                next()
            }
            else {
                response['status'] = 'fail';
                response.is_logout = true;
                response['errMsg'] = message.loginFailed;
                res.json(response);
                return;
            }
        })
        .catch((err) => {
	       logger.error(err);	
            response['status'] = 'fail';
            response['errMsg'] = message.apiFailed;
            res.json(response);
            return;
        })
    }
})

userRoute(app , space_model.connect , space_model.iamporter , logger);
utilRoute(app , space_model.connect , logger);
serviceRoute(app , space_model.connect , space_model.iamporter , logger);
mypageRoute(app , space_model.connect , space_model.iamporter , logger);

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

//var server = require('http').createServer(app);
var server = require('https').createServer(credentials , app);

server.listen(port, function() {
    console.log('space RESTful API server started on: ' + port);
});
